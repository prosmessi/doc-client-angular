import { NgModule } from '@angular/core';
import {ClickOutsideDirective} from './outside-click.directive';

@NgModule({
    declarations: [
        ClickOutsideDirective
    ],
    exports     : [
        ClickOutsideDirective
    ]
})
export class OutsideClickModule
{
}
