import {Directive, Output, HostListener, ElementRef, EventEmitter} from '@angular/core';

@Directive({
    selector: '[appClickOutside]'
})
export class ClickOutsideDirective {

    @Output() clickElsewhere = new EventEmitter<MouseEvent>();

    constructor(public eRef: ElementRef) {
    }

    @HostListener('document:click', ['$event']) onDocumentClick(event): void {
        console.log('[outside-click.directive.ts || Line no. 14 ....]', 'Elsewhere hit...');
        this.clickElsewhere.emit();
    }

    @HostListener('click',['$event']) onClick(e: MouseEvent): void {
        e.stopPropagation();
    }

}
