import {animate, style, transition, trigger} from '@angular/animations';

const slideInAnimation = trigger('slideInAnimation',
	[
		// Prevent the transition if the state is false
		transition('void => *', [
			style({opacity: 0, height: 0}),
			animate('200ms cubic-bezier(.78,.29,.28,.76)', style({opacity: 1, height: '*'}))
		]),
		transition('* => void', [
			style({opacity: 1, height: '*'}),
			animate('200ms cubic-bezier(.78,.29,.28,.76)', style({opacity: 0, height: 0}))
		])
	]
);

export default slideInAnimation;
