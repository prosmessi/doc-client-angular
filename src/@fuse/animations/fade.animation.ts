import {animate, group, style, transition, trigger} from '@angular/animations';

export const fadeAnimation = trigger('fadeAnimation', [
    transition('void => *', [
        style({opacity: 0}),
        group([
            animate('150ms ease-out', style({opacity: 1})),
        ])
    ]),
    transition('* => void', [
        style({opacity: 1}),
        animate('150ms ease-out', style({opacity: 0}))
    ])
]);
