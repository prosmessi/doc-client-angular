// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can b baseUrl: 'https://deshclinic.com:3001/',e found in `angular.json`.

export const environment = {
    production: false,
    // baseUrl: 'https://deshclinic.com:3001/',
    // baseUrl: 'https://sisgain.com:3001/',
    baseUrl: 'https://192.168.1.43:3001/',
    baseImageUrl: 'https://sisgain.com/project/la_bike_api/'
    // baseUrl: 'https://13.233.26.215:3001/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
