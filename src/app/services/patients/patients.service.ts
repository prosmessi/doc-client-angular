import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { AppUrlsService } from "../../core/app-urls/app-urls.service";
import { Observable } from "rxjs";
import { catchError } from "rxjs/operators";
import { handleError } from "../error-handler/error-handler.service";

@Injectable({
    providedIn: 'root'
})
export class PatientsService {

    _patientData: any;

    constructor(
        private _httpClient: HttpClient,
        private _appUrlService: AppUrlsService
    ) {
    }

    /**
     * Get Patients
     *
     * @returns
     */
    getPatients(page?, limit?, campId?): Observable<any> {
        return this._httpClient.get(this._appUrlService.patients(page, limit, campId)).pipe(
            catchError(handleError)
        );
    }

    /**
     * Get Patients
     *
     * @returns
     */
    getPatientsSearch(page?, limit?, name?): Observable<any> {
        return this._httpClient.get(this._appUrlService.patientsSearch(page, limit, name)).pipe(
            catchError(handleError)
        );
    }

    /**
     * getPatientsPastappointment
     */
    getPatientsPastappointment(patient_id?, page?, limit?): Observable<any> {
        return this._httpClient.get(this._appUrlService.getPatientsPastappointment(patient_id, page, limit)).pipe(
            catchError(handleError)
        );
    }
    /**
   * ADD Doctor
   * @returns {observable<any>}
   */
    addPatient(data): Observable<any> {
        return this._httpClient.post(this._appUrlService.addPatient(), data).pipe(
            catchError(handleError)
        );
    }
    /**
     * ADD Doctor
     * @returns {observable<any>}
     */
    updatePatient(data): Observable<any> {
        console.log(data);
        const patientId = data.uuid;
        return this._httpClient.put(this._appUrlService.updatePatient(patientId), data).pipe(
            catchError(handleError)
        );
    }

    /**
   * ADD Doctor
   * @returns {observable<any>}
   */
    makeCall(data): Observable<any> {
        return this._httpClient.post(this._appUrlService.makeCall(), data).pipe(
            catchError(handleError)
        );
    }

}
