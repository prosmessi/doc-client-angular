import {Injectable} from '@angular/core';

@Injectable()
export class StorageService {

    constructor() {
    }

    static getItem(key): any {
        return localStorage.getItem(key) ? JSON.parse(localStorage.getItem(key)) : null;
    }

    static setItem(key, value): any {
        localStorage.setItem(key, JSON.stringify(value));
    }

    static removeItem(key): any {
        localStorage.removeItem(key);
    }

    static clearAll(): any {
        localStorage.clear();
    }

}
