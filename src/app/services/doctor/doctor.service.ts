import {Injectable} from '@angular/core';
import {AppUrlsService} from '../../core/app-urls/app-urls.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {handleError} from '../error-handler/error-handler.service';

@Injectable({
    providedIn: 'root'
})
export class DoctorService {

    _doctorData: any;

    constructor(
        private _httpClient: HttpClient,
        private _appUrlService: AppUrlsService
    ) {
    }

    /**
     * Get Doctor
     *
     * @returns
     */
    getDoctors(page?, limit?,campId?): Observable<any> {
        return this._httpClient.get(this._appUrlService.doctor(page, limit,campId)).pipe(
            catchError(handleError)
        );
    }

    /**
     * Get Doctor
     *
     * @returns
     */
    getDoctorRating(page, limit, doctorId): Observable<any> {
        return this._httpClient.get(this._appUrlService.getDoctorRating(page, limit, doctorId)).pipe(
          catchError(handleError)
        );
    }
     /**
     * Get Doctor
     *
     * @returns
     */
    getdoctorSearch(page?, limit?,spec?,name?): Observable<any> {
        return this._httpClient.get(this._appUrlService.doctorSearch(page, limit,spec,name)).pipe(
            catchError(handleError)
        );
    }
    // /**
    //  * Get Doctor
    //  *
    //  * @returns
    //  */
    // getUserSearch(page?, limit?, queryField?): Observable<any> {
    //     return this._httpClient.get(this._appUrlService.getUserSearch(page, limit, queryField));
    // }
      /**
     * Get Doctor
     *
     * @returns
     */
    getdrugsSearch(page?,limit?,name?): Observable<any> {
        return this._httpClient.get(this._appUrlService.drugsSearch(page,limit,name)).pipe(
            catchError(handleError)
        );
    }
     /**
     * Get Representative
     *
     * @returns
     */
    getagent(page?, limit?): Observable<any> {
        return this._httpClient.get(this._appUrlService.agent(page, limit)).pipe(
            catchError(handleError)
        );
    }

    /**
     * Get Doctor
     *
     * @returns
     */
    getSpeciality(page?,limit?): Observable<any> {
        return this._httpClient.get(this._appUrlService.activeSpeciality(page,limit)).pipe(
            catchError(handleError)
        );
    }

    /**
     * Get Doctor
     *
     * @returns
     */
    getCity(state): Observable<any> {
        return this._httpClient.get(this._appUrlService.city(state)).pipe(
            catchError(handleError)
        );
    }

    /**
     * Get Doctor
     *
     * @returns
     */
    getState(): Observable<any> {
        return this._httpClient.get(this._appUrlService.state()).pipe(
            catchError(handleError)
        );
    }


    /**
     * Delete Doctor
     *
     * @param doctorId: string
     * @returns {Observable<any>}
     */

    deleteDoctors(doctorId: string): Observable<any> {
         return this._httpClient.delete(this._appUrlService.deleteuser(doctorId)).pipe(
             catchError(handleError)
         );
     }

    /**
     * ADD Doctor
     * @returns {observable<any>}
     */
    addDoctor(data): Observable<any> {
        return this._httpClient.post(this._appUrlService.addDoctor(), data).pipe(
            catchError(handleError)
        );
    }

     /**
     * Create Pdf
     * @returns {observable<any>}
     */
    createPdf(data): Observable<any> {
        return this._httpClient.post(this._appUrlService.createPdf(), data).pipe(
            catchError(handleError)
        );
    }


    /**
     * update availability
     * @returns {observable<any>}
     */
    addavailability​(uuid,data): Observable<any> {
        return this._httpClient.put(this._appUrlService.updateAvailability(uuid), data).pipe(
            catchError(handleError)
        );
    }

     /**
     * ADD Agent
     * @returns {observable<any>}
     */
    addAgent(data): Observable<any> {
        return this._httpClient.post(this._appUrlService.addAgent(), data).pipe(
            catchError(handleError)
        );
    }


    /**
     * Update Doctor
     * @returns {observable<any}
     */
    updateDoctor(data): Observable<any> {
        const doctorId = data.uuid;
       // delete data['_id'];
        return this._httpClient.put(this._appUrlService.doctorId(doctorId), data).pipe(
            catchError(handleError)
        );
    }
     /**
     * Update Doctor
     * @returns {observable<any}
     */
    updateAgent(data): Observable<any> {
        const doctorId = data.uuid;
       // delete data['_id'];
        return this._httpClient.put(this._appUrlService.agentId(doctorId), data).pipe(
            catchError(handleError)
        );
    }
}
