import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {FormGroup} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class PaginationService {


  public totalItemsCount = 0;
  public limit = 10;
  public currentPage = 1;
  public currentItems = [];
  public queryField = '';

  public loadingState$: Subject<boolean> = new Subject();
  public fetchState$: Subject<any> = new Subject();

  private loadedData = [];

  constructor() { }

  public async fetchForFirstTime(): Promise<void> {
    await this.fetchFromDB(this.currentPage - 1, this.limit);
    this.currentItems = this.loadedData;
    // this.getSearchedDoctors(this.currentPage, this.limit);
  }

  /**
   * @description Call after you have successfully query your database
   * @param index
   */
  public async deleteItem(index): Promise<void> {
    const noLoad = this.loadedData.length === this.totalItemsCount;
    this.currentItems = this.currentItems.filter((c, ind) => ind !== index);
    this.loadedData = this.loadedData.filter((c, ind) => ((this.currentPage - 1) * this.limit)  + index !== ind);

    if(noLoad) {
      this.totalItemsCount -= 1;
      return;
    }
    // If the items are not loaded ahead for the filling process after deletion
    const isLoaded = this.currentPage * this.limit < this.loadedData.length;
    console.log('isLoaded: ', isLoaded);
    if(!isLoaded) {
      await this.fetchFromDB(this.currentPage * this.limit - 1, 1);
    } else {
      this.totalItemsCount -= 1;
    }
    console.log('[pagination.class.ts || Line no. 51 ....]', this.totalItemsCount, this.loadedData);
    this.currentItems = [...this.currentItems, this.loadedData[this.currentPage * this.limit - 1]];
    // Then fetch from the database
    // Otherwise just load that item
  }

  public async onPageSizeChange(from, to): Promise<void> {
      // let index = this.currentPage * from;
      // console.log(index, newPageAtIndex, from > to);
      const index = (this.currentPage - 1) * this.limit + 1;
      const newPageAtIndex = Math.ceil(index / to);

      // Bug here on current page when going from 10 to 5 at 45-53 [Fixed]
      // if (from > to) newPageAtIndex = this.currentPage;

      this.limit = to;
      this.currentPage = newPageAtIndex;

      // Handle if items are not loaded and starting from new index
      // The next items are not fetched yet from the db
      const cond =
          this.loadedData.length < this.currentPage * to &&
          this.loadedData.length < this.totalItemsCount;

      // console.log(this.currentPage);

      if (cond) {
          const startInd = this.loadedData.length;
          // const end = this.currentPage * to;
          // Bug spotted here [Fixed]
          const arr = await this.fetchFromDB(startInd, this.limit);
          // console.log(arr);
          // this.loadedData.push(...arr);
      }

      const start = (this.currentPage - 1) * this.limit;
      const end = this.currentPage * this.limit;

      // console.log(start, end, this.currentPage);
      this.pushToArr({ start, end });
  }

  public async onPageChange(pageObj): Promise<void> {
    if (pageObj.pageSize !== this.limit) {
      await this.onPageSizeChange(this.limit, pageObj.pageSize);
      return;
    }
    if (pageObj.pageIndex - pageObj.previousPageIndex > 0) {
      await this.next();
    } else {
      await this.prev();
    }
    this.currentPage = pageObj.pageIndex + 1;
  }

  public async handleSearchClick(form: FormGroup): Promise<void> {
    console.log('[pagination.service.ts || Line no. 80 ....]', form);
    this.loadingState$.next(true);
    this.currentPage = 1;
    this.loadedData = [];
    // this.currentItems = [];
    this.prepareQuery(form);
    await this.fetchFromDB(this.currentPage - 1, this.limit);
    this.currentItems = this.loadedData;
  }

  private pushToArr({ start, end }): void {
    // console.log(this.loadedData);
    // return;
    this.currentItems = [];
    this.currentItems = this.loadedData.slice(start, end);
    // console.log(this.currentItems);
  }

  private async next(): Promise<void> {
      const cond =
          this.loadedData.length >= (this.currentPage + 1) * this.limit ||
          this.loadedData.length === this.totalItemsCount;
      // console.log((this.currentPage + 1) * this.pageSize, cond, this.currentPage);
      const start = this.currentPage * this.limit;
      const end = (this.currentPage + 1) * this.limit;
      if (!cond) {
          // Logic for iterating one by one
          // Change it in future to jump
          const arr = await this.fetchFromDB(start, this.limit);
          // console.log(arr);
          // this.loadedData.push(...arr);
      }
      // return;
      // console.log(this.loadedData);
      this.pushToArr({ start, end });
  }

  private prev(): void {
      const start = (this.currentPage - 2) * this.limit;
      const end = (this.currentPage - 1) * this.limit;
      this.pushToArr({ start, end });
  }

  private prepareQuery(form: FormGroup): void {
    let query = '';
    Object.keys(form.controls).forEach((controlName) => {
      const value = form.get(controlName).value;
      if(value) {
        query += `${controlName}=${value}&`;
      }
    });
    this.queryField = query;
    console.log('[pagination.service.ts || Line no. 132 ....]', query);
  }

  private async fetchFromDB(offset, limit): Promise<void> {
    console.log('Fetching from db ');

    this.loadingState$.next(true);

    const params = { page: offset, limit };
    return new Promise((res, rej) => {
      this.fetchState$.next({cb: (data) => {
        this.loadingState$.next(false);

        console.log('[doctors-list.component.ts || Line no. 171 ....]', data);

        if (data.status_code === 200) {
          this.totalItemsCount = data['result'].count;
          this.loadedData.push(...data['result'].data);
        }
        if (data.status_code === 404) {
          if(this.loadedData.length === 0) {
            this.totalItemsCount = 0;
          }
        }
        res();
      }, params});
      });
  }
}
