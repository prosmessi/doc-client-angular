import {Injectable} from '@angular/core';
import {Subject, BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
import { map, startWith, switchMap } from 'rxjs/operators';
import {AppUrlsService} from '../../core/app-urls/app-urls.service';
import { merge } from 'rxjs';
import {catchError} from 'rxjs/operators';
import {handleError} from '../error-handler/error-handler.service';
import {FormGroup} from '@angular/forms';
import {environment} from '../../../environments/environment';

export interface IValidationMessageObj {
    ifPattern: (msg: string) => string;
    message: string;
}

@Injectable({
    providedIn: 'root'
})
export class GlobalService {

    _matSnackBarConfig: MatSnackBarConfig;
    _allStates = [];
    _allCities = [];
    _serverUrl = environment.baseUrl;
    _baseImageUrl = environment.baseImageUrl;

    constructor(
        private _matSnackBar: MatSnackBar,
        private _httpClient: HttpClient,
        private _appUrlService: AppUrlsService
    ) {
        this.setMatSnackBarConfig();
    }

    /**
     * Set Global Mat SnackBar Position and layout
     *
     * @return
     */
    setMatSnackBarConfig(): void {
        this._matSnackBarConfig = {
            duration: 3000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: 'green-bg'
        };
    }

    /**
     * Show message globally
     *
     * @return
     */
    showMessage(message: string, panelClass = 'green-snackbar'): void {
        this._matSnackBarConfig.panelClass = panelClass;
        this._matSnackBar.open(message, '', this._matSnackBarConfig);
    }

    /**
     * Show message globally
     *
     * @return {void}
     */
    showError(message: string, panelClass = 'red-snackbar'): void {

        this._matSnackBarConfig.panelClass = panelClass;
        this._matSnackBar.open(message, '', this._matSnackBarConfig);
    }

    /**
     * Setting State
     *
     */

    setState(): void {
        merge().pipe(
            startWith({}),
            switchMap(() => {
                return this._httpClient.get(this._appUrlService.state()).pipe(
                    catchError(handleError)
                );
            }),
            map((data) => {
                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                this._allStates = data;
            },
            (err) => {}
        );
    }

    setCities(state): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                return this._httpClient.get(this._appUrlService.city(state)).pipe(
                    catchError(handleError)
                );
            }),
            map((data) => {
                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }
            })
        ).subscribe(
            (data) => {
                this._allCities = data;
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }

    toSentenceCase(res: string): string { return res.charAt(0).toUpperCase() + res.substring(1).toLowerCase(); }

    /**
     * Get User by role
     *
     * @returns
     */
    getUserSearch(page?, limit?, queryField?): Observable<any> {
        return this._httpClient.get(this._appUrlService.userSearch(page, limit, queryField));
    }
    
    
    getCondition(form: FormGroup): (formControlName: string) => boolean {
        return (formControlName: string): boolean => form.get(formControlName).status === 'INVALID' && (form.get(formControlName).dirty || form.get(formControlName).touched);
    }

    getValidationMessage(form: FormGroup): (formControlName: string) => IValidationMessageObj {
        return (formControlName: string): IValidationMessageObj => {
            let isPattern = false;
            let message = 'Invalid field';
            if(form.get(formControlName).errors?.pattern) {
                // TODO: Assuming the pattern is only for checking number [Temporary]
                message = 'Must only contain digits';
                isPattern = true;
            }
            if(form.get(formControlName).errors?.maxlength) {
                message = 'Seems too big';
            }
            if(form.get(formControlName).errors?.required) {
                message = 'Required field';
            }
            if(form.get(formControlName).errors?.minlength) {
                message = 'Seems too short';
            }
            if(form.get(formControlName).errors?.email) {
                message = 'Invalid email';
            }

            return {
                ifPattern: (msg?: string): string => {
                    if (isPattern && msg) {return msg;}
                    return message;
                },
                message
            };
        };
    }
    /**
     * Get User by role
     *
     * @returns
     */
    searchCallLogs(page?, limit?, queryField?): Observable<any> {
        return this._httpClient.get(this._appUrlService.searchCallLogs(page, limit, queryField));
    }
    /**
     * Get User by role
     *
     * @returns
     */
    searchRatings(page?, limit?, queryField?): Observable<any> {
        return this._httpClient.get(this._appUrlService.searchRatings(page, limit, queryField));
    }

}
