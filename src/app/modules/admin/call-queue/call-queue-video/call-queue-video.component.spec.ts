import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CallQueueVideoComponent } from './call-queue-video.component';

describe('CallQueueVideoComponent', () => {
  let component: CallQueueVideoComponent;
  let fixture: ComponentFixture<CallQueueVideoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CallQueueVideoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CallQueueVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
