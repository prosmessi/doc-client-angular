import { Component, OnInit } from '@angular/core';
import {AppointmentService} from '../../../../services/appointment/appointment.service';
import {Router} from '@angular/router';
import {merge} from 'rxjs';
import {map, startWith, switchMap} from 'rxjs/operators';
@Component({
  selector: 'app-call-queue-list',
  templateUrl: './call-queue-list.component.html',
  styleUrls: ['./call-queue-list.component.scss']
})
export class CallQueueListComponent implements OnInit {

    isLoading: boolean = false;
    callLogsTableColumns: string[] = ['name', 'phone', 'date', 'checkIn', 'action'];
    callLogs :any=[];
    callLogsCount: number = 1;
    loggedInUser: any;
    uuid: string;

    constructor(
        private _appointmentService: AppointmentService,
        private _router: Router
    ) {
    }

    ngOnInit(): void {
        // this.callLogsCount = 1;
        this.loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));
        this.uuid = this.loggedInUser.uuid;
        this.callWaitingList​();
    }

    /**
     * Preview doctor's info
     *
     * @param doctor
     */
    previewDetails(doctor): void {
        localStorage.setItem('listItem', JSON.stringify(doctor));
        this._appointmentService._data = doctor;
        this._router.navigate([`/call-queue/preview/${doctor.id}/video`]);
    }

    /**
     * joincall
     */
     joincall(doctor): void {
         // debugger
        localStorage.setItem('listItem', JSON.stringify(doctor));
        this._appointmentService._data = doctor;
        this._router.navigate([`/call-queue/preview/${doctor.id}/video`]);
    }
       //callWaitingList
       callWaitingList(): void {
           // debugger
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    uuid : this.uuid,
                    page: 1,
                    limit: 20,
                };
                return this._appointmentService.callWaitingList(params.uuid);
            }),
            map((data) => {
              console.log('[call-queue-list.component.ts || Line no. 69 ....]', data);
                this.isLoading = false;
                // set pagination total count
                this.callLogsCount = data['result'].length;
                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }

            })
        ).subscribe(
            (data) => {
                // set response data
                // debugger
                this.callLogs = data;
                console.log(this.callLogs);
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
}
}
