import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import {
    AbstractControl,
    FormBuilder,
    FormGroup,
    NgForm,
    ValidationErrors,
    ValidatorFn,
    Validators
} from '@angular/forms';
import { FuseAlertType } from '../../../../@fuse/components/alert';
import { AuthService } from '../../../core/auth/auth.service';
import { fuseAnimations } from '../../../../@fuse/animations';
import theme from '@fuse/theme';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ResetPasswordComponent implements OnInit {

    @ViewChild('forgotPasswordNgForm') forgotPasswordNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type: 'success',
        message: ''
    };
    resetPasswordForm: FormGroup;
    showAlert: boolean = false;
    isLoading = false;
    public theme = theme;

    /**
     * Constructor
     */
    constructor(
        private _authService: AuthService,
        private _formBuilder: FormBuilder
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Create the form
        this.resetPasswordForm = this._formBuilder.group({
            oldPassword: ['', [Validators.required]],
            newPassword: ['', [Validators.required]],
            confirmPassword: ['', [Validators.required, this.confirmPassword()]]
        });
    }

    confirmPassword(): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
            // console.log(control.parent?.value?.password);
            const pass = control.parent?.value?.newPassword;
            const confirmPass = control.value;
            return pass !== confirmPass ? {notMatch: true} : null;
            // return {notMatch: true};
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    resetPassword(): void {
        if (this.resetPasswordForm.invalid) {
            return;
        }
        this.isLoading = true;
        // Disable the form
        this.resetPasswordForm.disable();

        this._authService.changeByOldPassword(this.resetPasswordForm.value)
          .subscribe(
            (res) => {
                this.isLoading = false;
                this.resetPasswordForm.enable();
                this.resetPasswordForm.reset();
                console.log('[forgot-password.component.ts || Line no. 71 ....]', res);
                if (res.status_code === 200) {
                    // const redirectURL = this._activatedRoute.snapshot.queryParamMap.get('redirectURL') || '/dashboard';
                    this.alert = {
                        type: 'success',
                        message: res.status_message
                    };
                    this.showAlert = true;

                    // setTimeout(() => {
                    //     this._router.navigateByUrl('/sign-in');
                    // }, 1500);
                } else {
                    this.alert = {
                        type: 'error',
                        message: res.status_message
                    };
                    this.showAlert = true;
                    // Setting the default value for the type as there is no option to reselect the role
                    // this.forgotPasswordForm.controls['type'].patchValue('admin');
                }

            },
            (response) => {
                this.isLoading = false;
                console.log('[forgot-password.component.ts || Line no. 89 ....]', response);
                // Re-enable the form
                this.resetPasswordForm.enable();

                // Reset the form
                this.resetPasswordForm.reset();
                this.resetPasswordForm.setErrors(null);
                // Setting the default value for the type as there is no option to reselect the role
                // this.forgotPasswordForm.controls['type'].patchValue('admin');

                // Set the alert
                this.alert = {
                    type: 'error',
                    message: 'Something went wrong, Please try again later'
                };

                // Show the alert
                this.showAlert = true;
            }
          );
    }

}
