import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './dashboard.component';
import {Route, RouterModule} from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import {NgApexchartsModule} from "ng-apexcharts";
import {ChartsModule} from "ng2-charts";
import {NgxChartsModule} from "@swimlane/ngx-charts";
import {CdkScrollableModule} from "@angular/cdk/scrolling";
import {MatTableModule} from "@angular/material/table";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {_MatMenuDirectivesModule, MatMenuModule} from "@angular/material/menu";

const routes: Route[] = [
    {
        path: '',
        component: DashboardComponent
    }
];

@NgModule({
    declarations: [
        DashboardComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        NgApexchartsModule,
        HttpClientModule,
        ChartsModule,
        NgxChartsModule,
        CdkScrollableModule,
        MatTableModule,
        MatIconModule,
        MatButtonModule,
        _MatMenuDirectivesModule,
        MatMenuModule,
    ]
})
export class DashboardModule {
}
