import {Component, OnDestroy, OnInit} from '@angular/core';
import {ApexOptions} from 'ng-apexcharts';
import {Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {ProjectService} from './project.service';
import {ClinicService} from '../../../services/clinic/clinic.service';
import { environment } from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';

import * as moment from 'moment';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

    //chartGithubIssues: ApexOptions = {};

    todayDate = moment().toString();
    userInfo: any;
    data: any;
    role: string;
    isLoading: boolean;
    dashboardCount = {
        doctor: 0,
        patient: 0,
        admin: 0,
        c_appointment: 0,
        Pa_appointment: 0,
        up_appointment: 0,
        camp: 0,
    };

    graphConfig = {
        visits: {
            value: 882,
            ofTarget: -9
        },
        chartType: 'bar',
        datasets: [
            {
                label: 'Old',
                backgroundColor: '#F49BA9',
                data: [35, 25]
            },
            {
                label: 'New',
                backgroundColor: '#EE4862',
                data: [40, 3]
            },
        ],
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
        colors: [
            {
                borderColor: '#EE4862',
                backgroundColor: '#F49BA9'
            }
        ],
        options: {
            // barValueSpacing: 20,
            scales: {
                xAxes: [
                    {
                        gridLines: {
                            display: false,
                            drawBorder: false,
                            tickMarkLength: 18
                        },
                        ticks: {
                            fontColor: '#000000'
                        }
                    }
                ],
                yAxes: [
                    {
                        gridLines: {
                            display: false,
                            drawBorder: false,
                            tickMarkLength: 18
                        },
                        ticks: {
                            fontColor: '#000000'
                        }
                    },
                ]
            }
        }
        /* options: {
             spanGaps: false,
             legend: {
                 display: false
             },
             maintainAspectRatio: false,
             layout: {
                 padding: {
                     top: 32,
                     left: 32,
                     right: 32
                 }
             },
             elements: {
                 point: {
                     radius: 4,
                     borderWidth: 2,
                     hoverRadius: 4,
                     hoverBorderWidth: 2
                 },
                 line: {
                     tension: 0
                 }
             },
             scales: {
                 xAxes: [
                     {
                         gridLines: {
                             display: false,
                             drawBorder: false,
                             tickMarkLength: 18
                         },
                         ticks: {
                             fontColor: '#000000'
                         }
                     }
                 ],
                 yAxes: [
                     {
                         display: true,
                     }
                 ]
             },
             plugins: {
                 filler: {
                     propagate: false
                 },
                 xLabelsOnTop: {
                     active: true
                 }
             }
         }*/
    };


    commonOption: any = {
        responsive: true,
        maintainAspectRatio: false,
        cutoutPercentage: 60,
        backgroundColor: '#FAA503',
        borderColor: '#9ebecd',
    };

    colors = [{
        borderColor: '#FAA503',
        backgroundColor: '#FAA503',
        pointBackgroundColor: '#FAA503',
        pointHoverBackgroundColor: '#FAA503',
        pointBorderColor: '#ffffff',
        pointHoverBorderColor: '#ffffff'
    }];

    salesDataset = [{
        label: 'This week',
        data: [30, 40, 50, 15, 60, 20, 40],
        backgroundColor: '#FAA503',
        borderColor: '#FAA503'
    }];

    doctorGraphConfig = {
        visits: {
            value: 882,
            ofTarget: -9
        },
        chartType: 'bar',
        datasets: [
            {
                label: 'Old',
                backgroundColor: '#FAA503',
                data: [35]
            },
            {
                label: 'New',
                backgroundColor: '#FAA503',
                data: [40]
            },
        ],
        labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        colors: [
            {
                borderColor: '#FAA503',
                backgroundColor: '#F49BA9'
            }
        ],
        options: {
            // barValueSpacing: 20,
            scales: {
                xAxes: [
                    {
                        gridLines: {
                            display: false,
                            drawBorder: false,
                            tickMarkLength: 18
                        },
                        ticks: {
                            fontColor: '#000000'
                        }
                    }
                ],
                yAxes: [
                    {
                        gridLines: {
                            display: false,
                            drawBorder: false,
                            tickMarkLength: 18
                        },
                        ticks: {
                            fontColor: '#000000'
                        }
                    },
                ]
            }
        }
        /* options: {
             spanGaps: false,
             legend: {
                 display: false
             },
             maintainAspectRatio: false,
             layout: {
                 padding: {
                     top: 32,
                     left: 32,
                     right: 32
                 }
             },
             elements: {
                 point: {
                     radius: 4,
                     borderWidth: 2,
                     hoverRadius: 4,
                     hoverBorderWidth: 2
                 },
                 line: {
                     tension: 0
                 }
             },
             scales: {
                 xAxes: [
                     {
                         gridLines: {
                             display: false,
                             drawBorder: false,
                             tickMarkLength: 18
                         },
                         ticks: {
                             fontColor: '#000000'
                         }
                     }
                 ],
                 yAxes: [
                     {
                         display: true,
                     }
                 ]
             },
             plugins: {
                 filler: {
                     propagate: false
                 },
                 xLabelsOnTop: {
                     active: true
                 }
             }
         }*/
    };

    chartGithubIssues: ApexOptions = {
        chart: {
            fontFamily: 'inherit',
            foreColor: 'inherit',
            height: '100%',
            type: 'line',
            toolbar: {
                show: false
            },
            zoom: {
                enabled: false
            }
        },
        colors: ['#64748B', '#94A3B8'],
        dataLabels: {
            enabled: true,
            enabledOnSeries: [0],
            background: {
                borderWidth: 0
            }
        },
        grid: {
            borderColor: 'var(--fuse-border)'
        },
        labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        legend: {
            show: false
        },
        plotOptions: {
            bar: {
                columnWidth: '50%'
            }
        },
        states: {
            hover: {
                filter: {
                    type: 'darken',
                    value: 0.75
                }
            }
        },
        stroke: {
            width: [3, 0]
        },
        tooltip: {
            followCursor: true,
            theme: 'dark'
        },
        xaxis: {
            axisBorder: {
                show: false
            },
            axisTicks: {
                color: 'var(--fuse-border)'
            },
            labels: {
                style: {
                    colors: 'var(--fuse-text-secondary)'
                }
            },
            tooltip: {
                enabled: false
            }
        },
        yaxis: {
            labels: {
                offsetX: -16,
                style: {
                    colors: 'var(--fuse-text-secondary)'
                }
            }
        }
    };

    /* Doctor Dashboard */

    appointmentsCount: number = 0;

    appointmentsTableColumns: string[] = ['name', 'date', 'time', 'status', 'action'];
    appointments = [
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            speciality: 'Test',
            experience: 10,
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true,
            time: '11:30 AM'
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            speciality: 'Test',
            experience: 10,
            status: 'Inactive',
            createdAt: 'May 24 2021',
            action: true,
            time: '11:30 AM'
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            speciality: 'Test',
            experience: 10,
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true,
            time: '11:30 AM'
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            speciality: 'Test',
            experience: 10,
            status: 'Inactive',
            createdAt: 'May 24 2021',
            action: true,
            time: '11:30 AM'
        },

    ];

    private _unsubscribeAll: Subject<any> = new Subject<any>();

    constructor(
        private _projectService: ProjectService,
        private _router: Router,
        private _clinicService: ClinicService
    ) {
        this.isLoading = true;
    }

    /**
     * On init
     */
    ngOnInit(): void {

        this.userInfo = JSON.parse(localStorage.getItem('loggedInUser'));
        // Get the loggedIn user Role
        setTimeout(() => {
            this.isLoading = false;
        }, 2000);
        this.role = localStorage.getItem('role');

        this.appointmentsCount = 1;
        // Get the data
        this._projectService.data$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((data) => {
                // Store the data
                this.data = data;
                // Prepare the chart data
                this._prepareChartData();
            });

        // TODO: Refactor this to service
        // this.http.get(environment.baseUrl + 'admin/dashboard/count').subscribe((data) => {
        //     console.log('[dashboard.component.ts || Line no. 434 ....]', data);
        // });
        this._clinicService.getDashboardCount().subscribe((data) => {
            console.log('[dashboard.component.ts || Line no. 447 ....]', data);
            this.dashboardCount = data.result.counts;
            console.log('[dashboard.component.ts || Line no. 448 ....]', this.dashboardCount);
        });

        // Attach SVG fill fixer to all ApexCharts
        window['Apex'] = {
            chart: {
                events: {
                    mounted: (chart: any, options?: any): void => {
                        this._fixSvgFill(chart.el);
                    },
                    updated: (chart: any, options?: any): void => {
                        this._fixSvgFill(chart.el);
                    }
                }
            }
        };
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Fix the SVG fill references. This fix must be applied to all ApexCharts
     * charts in order to fix 'black color on gradient fills on certain browsers'
     * issue caused by the '<base>' tag.
     *
     *
     * @param element
     * @private
     */
    private _fixSvgFill(element: Element): void {
        // Current URL
        const currentURL = this._router.url;

        // 1. Find all elements with 'fill' attribute within the element
        // 2. Filter out the ones that doesn't have cross reference so we only left with the ones that use the 'url(#id)' syntax
        // 3. Insert the 'currentURL' at the front of the 'fill' attribute value
        Array.from(element.querySelectorAll('*[fill]'))
            .filter(el => el.getAttribute('fill').indexOf('url(') !== -1)
            .forEach((el) => {
                const attrVal = el.getAttribute('fill');
                el.setAttribute('fill', `url(${currentURL}${attrVal.slice(attrVal.indexOf('#'))}`);
            });
    }

    /**
     * Prepare the chart data from the data
     *
     * @private
     */
    private _prepareChartData(): void {
        // Github issues
        // @ts-ignore

    }

}
