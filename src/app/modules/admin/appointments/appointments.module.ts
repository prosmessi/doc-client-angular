import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AppointmentsListComponent} from './appointments-list/appointments-list.component';
import {AppointmentsFormComponent} from './appointments-form/appointments-form.component';
import {AppointmentsPreviewComponent} from './appointments-preview/appointments-preview.component';
import {Route, RouterModule} from '@angular/router';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {CdkScrollableModule} from '@angular/cdk/scrolling';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatRadioModule} from '@angular/material/radio';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatNativeDateModule} from '@angular/material/core';
import {ListByDateComponent} from './list-by-date/list-by-date.component';
import {ListByDateViewComponent} from './list-by-date-view/list-by-date-view.component';
import {AppointmentService} from "../../../services/appointment/appointment.service";
import {SharedModule} from '../../../shared/shared.module';

const routes: Route[] = [
    {
        path: '',
        component: AppointmentsListComponent
    },
    {
        path: 'list-by-date',
        component: ListByDateComponent
    },
    {
        path: 'list-by-date/view',
        component: ListByDateViewComponent
    },
    {
        path: '**',
        component: AppointmentsListComponent
    }
];

@NgModule({
    declarations: [
        AppointmentsListComponent,
        AppointmentsFormComponent,
        AppointmentsPreviewComponent,
        ListByDateComponent,
        ListByDateViewComponent
    ],
	imports: [
		CommonModule,
		RouterModule.forChild(routes),
		MatProgressBarModule,
		MatButtonModule,
		MatIconModule,
		MatTableModule,
		CdkScrollableModule,
		MatPaginatorModule,
		MatRadioModule,
		MatFormFieldModule,
		MatDatepickerModule,
		MatInputModule,
		MatSelectModule,
		MatNativeDateModule,
		SharedModule,
	],
    providers: [
        AppointmentService
    ]
})
export class AppointmentsModule {
}
