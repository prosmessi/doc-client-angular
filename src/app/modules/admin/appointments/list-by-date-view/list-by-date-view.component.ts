import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-list-by-date-view',
    templateUrl: './list-by-date-view.component.html',
    styleUrls: ['./list-by-date-view.component.scss']
})
export class ListByDateViewComponent implements OnInit {

    listCount: number = 0;
    listTableColumns: string[] = ['patientInfo', 'consultantType', 'scheduleInfo', 'status', 'action'];
    lists = [
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Jenny Wilson',
            mrNo: 'MR. nym01',
            phone: 9999999999,
            consultantType: 'Online',
            scheduleInfo: '25 May, 2021',
            time: '11:00 AM',
            status: 'Visited',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Jenny Wilson',
            mrNo: 'MR. nym01',
            phone: 9999999999,
            consultantType: 'Online',
            scheduleInfo: '25 May, 2021',
            time: '11:00 AM',
            status: 'Visited',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Jenny Wilson',
            mrNo: '(MR. nym01)',
            phone: 9999999999,
            consultantType: 'Offline',
            scheduleInfo: '25 May, 2021',
            time: '11:00 AM',
            status: 'Visited',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Jenny Wilson',
            mrNo: '(MR. nym01)',
            phone: 9999999999,
            consultantType: 'Online',
            scheduleInfo: '25 May, 2021',
            time: '11:00 AM',
            status: 'Visited',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Jenny Wilson',
            mrNo: '(MR. nym01)',
            phone: 9999999999,
            consultantType: 'Online',
            scheduleInfo: '25 May, 2021',
            time: '11:00 AM',
            status: 'Visited',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Jenny Wilson',
            mrNo: '(MR. nym01)',
            phone: 9999999999,
            consultantType: 'Online',
            scheduleInfo: '25 May, 2021',
            time: '11:00 AM',
            status: 'Visited',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Jenny Wilson',
            mrNo: '(MR. nym01)',
            phone: 9999999999,
            consultantType: 'Online',
            scheduleInfo: '25 May, 2021',
            time: '11:00 AM',
            status: 'Visited',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Jenny Wilson',
            mrNo: '(MR. nym01)',
            phone: 9999999999,
            consultantType: 'Online',
            scheduleInfo: '25 May, 2021',
            time: '11:00 AM',
            status: 'Visited',
            action: true
        },

    ];

    constructor() {
    }

    ngOnInit(): void {
        this.listCount = 1;
    }

}
