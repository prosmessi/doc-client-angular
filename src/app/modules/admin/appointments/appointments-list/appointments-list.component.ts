import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {merge, Observable} from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { AppointmentService } from '../../../../services/appointment/appointment.service';
import { MatPaginator } from '@angular/material/paginator';
import theme from '@fuse/theme';
import {FormControl, FormGroup} from '@angular/forms';
import {PaginationService} from '../../../../services/pagination/pagination.service';
import {Pagination} from '../../../../shared/classes/pagination.class';
import {GlobalService} from '../../../../services/global/global.service';
import {ShowErrorService} from '../../../../services/show-error/show-error.service';

@Component({
    selector: 'app-appointments-list',
    templateUrl: './appointments-list.component.html',
    styleUrls: ['./appointments-list.component.scss'],
    providers: [PaginationService]
})
export class AppointmentsListComponent extends Pagination implements OnInit, AfterViewInit {
    public httpReq: (page: number, limit: number, queryField: string) => Observable<any>;

    isLoading = true;
    appointmentsTableColumns: string[] = ['sno', 'memberName', 'doctorName', 'scheduleInfo', 'consultantType', 'action'];
    appointments: Array<any> = [];
    doctorsCount: number = 0;
    role: string;
    public theme = theme;
    filterForm: FormGroup;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

    constructor(
        private _appointmentService: AppointmentService,
        public _paginationService: PaginationService,
        public _globalService: GlobalService,
        private _showErrorService: ShowErrorService,
    ) {
        super();
        this.httpReq = (page, limit, queryField): Observable<any> => this._appointmentService.searchAppointments(page, limit, queryField);

        this.role = localStorage.getItem('role');
    }

    ngOnInit(): void {
        // this.doctorsCount = 1;
        this.filterForm = new FormGroup({
            search: new FormControl(null, []),
            date: new FormControl(null, []),
        });
        this.isLoading = true;
    }

    /**
     * After View Init
     */
    async ngAfterViewInit(): Promise<void> {
        // this.getAppointmentsList();
        await this.fetchForFirstTime();
    }

    resetSearch(e): void {
        e.stopPropagation();
        this.filterForm.reset();
        this.isSearchedList = false;
        this.handleSearchClick(this.filterForm).then();
    }

    /**
     * Get Appointments List
     * return {Void}
     */

    getAppointmentsList(): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._appointmentService.getAppointments(params.page, params.limit, JSON.parse(localStorage.getItem('loggedInUser')).camp_id);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                this.doctorsCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                this.appointments = data;
                // debugger
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }


    /**
     * delete apointment
     *
     * @param agent
     */
    deleteAppointment(button,dataObj, index): void {

        this.isLoading = true;
        button.target.disabled = true;

        // const data = this.specialityForm.value;
        // const data = {
        //     id:dataObj.id
        // }
        this._appointmentService.deleteAppointment(dataObj).subscribe(
          () => {
              this._globalService.showMessage('Appointment Deleted Successfully');
              // this._matSnackBar.open('Appointment Deleted Successfully', 'OK', this._globalService._matSnackBarConfig);
              // this._router.navigate(['/speciality']);
              //    delete dataObj;
              this.deleteItem(index).then(() => {
                  this.isLoading = false;
                  button.target.disabled = false;
              });
              // this.appointments =  this.appointments.filter(person => person.appoint_id !=dataObj );
              //     setTimeout(() => {
              //         this.isLoading = false;
              //         button.target.disabled = false;
              //     }, 500);
          },
          (err) => {
              this._showErrorService.showError(err);
              setTimeout(() => {
                  this.isLoading = false;
                  button.target.disabled = false;
              }, 500);
          });
    }
}
