import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {DoctorService} from '../../../../services/doctor/doctor.service';
import {GlobalService} from '../../../../services/global/global.service';
import {ShowErrorService} from '../../../../services/show-error/show-error.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ClickOutsideDirective} from '../../../../../@fuse/directives/outside-click/outside-click.directive';
import * as moment from 'moment';
import theme from '@fuse/theme';


@Component({
  selector: 'app-availability-form',
  templateUrl: './availability-form.component.html',
  styleUrls: ['./availability-form.component.scss']
})
export class AvailabilityFormComponent implements OnInit {
  durations: any[] = [
    {
      key: 5,
      value: '5 Min'
    },
    {
      key: 10,
      value: '10 Min'
    },
    {
      key: 15,
      value: '15 Min'
    },
    {
      key: 20,
      value: "20 Min"
    },
    {
      key: 25,
      value: "25 Min"
    },
    {
      key: 30,
      value: "30 Min"
    },
    {
      key: 35,
      value: "35 Min"
    },
    {
      key: 40,
      value: "40 Min"
    },
    {
      key: 45,
      value: "45 Min"
    },
    {
      key: 50,
      value: "50 Min"
    },
    {
      key: 55,
      value: "55 Min"
    },
    {
      key: 60,
      value: "60 Min"
    }
  ];
  // Default Duration
  showToolTip: boolean;
  duration: number;
  days: any[] = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  timeslot: any[];
  timeslot1: any[];
  workingHours: any = [{
    dayname: 'Sunday',
    daystatus: false,
    session: [{
      fromTime: '',
      toTime: ''
    }]
  }, {
    dayname: 'Monday',
    daystatus: false,
    session: [{
      fromTime: '',
      toTime: ''
    }]
  }, {
    dayname: 'Tuesday',
    daystatus: false,
    session: [{
      fromTime: '',
      toTime: ''
    }]
  }, {
    dayname: 'Wednesday',
    daystatus: false,
    session: [{
      fromTime: '',
      toTime: ''
    }]
  }, {
    dayname: 'Thursday',
    daystatus: false,
    session: [{
      fromTime: '',
      toTime: ''
    }]
  }, {
    dayname: 'Friday',
    daystatus: false,
    session: [{
      fromTime: '',
      toTime: ''
    }]
  }, {
    dayname: 'Saturday',
    daystatus: false,
    session: [{
      fromTime: '',
      toTime: ''
    }]
  }];
  start = "09:00";
  end = "24:00";
  disabled = false;
  checked = false;
  isLoading: boolean;
  uuid: any;
  role: any;
  public theme = theme;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _doctorService: DoctorService,
    private _globalService: GlobalService,
    private _showErrorService: ShowErrorService,
    private _matSnackBar: MatSnackBar
  ) {
    // debugger
  }
  ngOnInit(): void {
    // debugger
    console.log('[availability-form.component.ts || Line no. 146 ....]', this.showToolTip);
    this.showToolTip = true;
    var loggedInUser = JSON.parse(localStorage.getItem('listItem'));
    this.uuid = loggedInUser.uuid;
    this.role = loggedInUser.user_type_id;
    var data = JSON.parse(loggedInUser.availability);
    console.log('Video consultation Duration', data);
    if(data) {
      this.showToolTip = false;
      this.duration = +data.videoCosulationduration;
      this.workingHours = data.videoCosulationworkingHours;
      this.durationchange();
    }
    // this.timeslot = this.getTimeStops('09:00', '24:00', this.duration);
  }
  tConvert(time) {
    // Check correct time format and split into components
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) { // If time format correct
      time = time.slice(1);  // Remove full string match value
      time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join(''); // return adjusted time or original string
  }

  hideToolTip() {
    console.log('[availability-form.component.ts || Line no. 173 ....]', 'Tool tip hidden');
    this.showToolTip = false;
  }

  onToggleDay(event, day) {
    console.log(event, this.workingHours[day]);
    // debugger
    if (event.checked &&
      this.workingHours[day].session.length === 1 &&
      !this.workingHours[day].session[0].fromTime.trim() &&
      !this.workingHours[day].session[0].toTime.trim()) {
      this.workingHours[day].session[0].fromTime = ""
      this.workingHours[day].session[0].toTime = ""
    }
  }
  onFromTimeChange(event, day, index) {
    if (this.workingHours[day].session[index].toTime && parseInt(event.source.value.replace(":", "")) >= parseInt(this.workingHours[day].session[index].toTime.replace(":", ""))) {
      event.source.value = null
    } else {
      this.workingHours[day].session.some((data1, i) => {
        if (i != index) {
          if (data1.fromTime && data1.toTime && parseInt(data1.fromTime.replace(":", "")) <= parseInt(event.source.value.replace(":", "")) &&
            parseInt(data1.toTime.replace(":", "")) > parseInt(event.source.value.replace(":", ""))) {
            event.source.value = null
            return true;
          } else {
          }
        }
      })
    }
  }
  onToTimeChange(event, day, index) {
    if (this.workingHours[day].session[index].fromTime && parseInt(event.source.value.replace(":", "")) <= parseInt(this.workingHours[day].session[index].fromTime.replace(":", ""))) {
      event.source.value = null
    } else {
      this.workingHours[day].session.some((data1, i) => {
        if (i != index) {
          if (data1.fromTime && data1.toTime && parseInt(data1.fromTime.replace(":", "")) < parseInt(event.source.value.replace(":", "")) &&
            parseInt(data1.toTime.replace(":", "")) >= parseInt(event.source.value.replace(":", ""))) {
            event.source.value = null;
            return true;
          } else if (this.workingHours[day].session[index].fromTime && parseInt(data1.fromTime.replace(":", "")) > parseInt(this.workingHours[day].session[index].fromTime.replace(":", "")) &&
            parseInt(data1.fromTime.replace(":", "")) < parseInt(event.source.value.replace(":", ""))) {
            event.source.value = null;
            return true;
          } else {
          }
        }
      })
    }
  }
  addMore(key) {
    if (this.workingHours[key].session.length < 3) {
      this.workingHours[key].session.push({
        fromTime: '',
        toTime: ''
      });
    }
  }
  removeAddress(key, length) {
    this.workingHours[key].session.splice(length, 1);
  }
  getTimeStops(start, end, interval) {
    var startTime = moment(start, 'HH:mm');
    var endTime = moment(end, 'HH:mm');
    if (endTime.isBefore(startTime)) {
      endTime.add(1, 'day');
    }
    var timeStops = [];
    while (startTime <= endTime) {
      timeStops.push(moment(startTime).format('HH:mm'));
      startTime.add(interval, 'minutes');
    }
    return timeStops;
  }
  durationchange() {
    console.log(this.duration);
    var startTime = moment(this.start, 'HH:mm');
    var endTime = moment(this.end, 'HH:mm');
    startTime.add(this.duration, 'minutes').format('HH:mm');
    this.timeslot = this.getTimeStops(this.start, this.end, this.duration);

    this.timeslot1 = this.getTimeStops(startTime, this.end, this.duration);
  }
  /**
   * Create New Doctor
   * return {Void} /doctor​/update​/availability​/:uuid
   */
  setAvailability(): void {

    // debugger
    var datastring = {
      videoCosulation: true,
      videoCosulationfee: "100",
      videoCosulationduration: this.duration,
      videoCosulationnoofdays: "2",
      videoCosulationaddCalender: "",
      videoCosulationfreeFollowUp: true,
      videoCosulationworkingHours: this.workingHours
    }
    var data = {
      availability: JSON.stringify(datastring),
      in_clinic_consultation: JSON.stringify(datastring),
      time_Off: JSON.stringify(datastring),
      role: JSON.stringify(this.role),
    };
    // console.log(data);
    this._doctorService.addavailability(this.uuid, data).subscribe(
      (result) => {
        if (result.status_code == 200) {
          this._globalService.showMessage('New Availability added');
          // this._matSnackBar.open('New Availability added', 'OK', this._globalService._matSnackBarConfig);
          this._router.navigate(['/doctors']);
        } else {
          this._globalService.showError(result.status_message);
          // this._matSnackBar.open(result.status_message, 'OK', this._globalService._matSnackBarConfig);
        }
        setTimeout(() => {
          this.isLoading = false;
        }, 500);
      },
      (err) => {
        this._showErrorService.showError(err);
        setTimeout(() => {
          this.isLoading = false;
        }, 500);
      }
    );
  }
}
