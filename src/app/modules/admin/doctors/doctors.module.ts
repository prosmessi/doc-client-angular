import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoctorsListComponent } from './doctors-list/doctors-list.component';
import { DoctorsFormComponent } from './doctors-form/doctors-form.component';
import { DoctorsPreviewComponent } from './doctors-preview/doctors-preview.component';
import { AvailabilityFormComponent } from './set-availability/availability-form.component';
import { Route, RouterModule } from '@angular/router';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { CdkScrollableModule } from '@angular/cdk/scrolling';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { DoctorService } from '../../../services/doctor/doctor.service';
import { DoctorSearchListComponent } from './doctor-search-list/doctor-search-list.component';
import { PrescriptionFormComponent } from './prescription/prescription-form.component';
import {OutsideClickModule} from '../../../../@fuse/directives/outside-click/outside-click.module';
import {SharedModule} from '../../../shared/shared.module';
import {BarRatingModule} from 'ngx-bar-rating';
const routes: Route[] = [
    {
        path: '',
        component: DoctorsListComponent
    },
    {
        path: ':handle',
        component: DoctorsFormComponent,

    },
    {
        path: 'search/list',
        component: DoctorSearchListComponent,

    },
    {
        path: 'create/prescription',
        component: PrescriptionFormComponent,

    },
    {
        path: ':handle/:doctorId',
        component: DoctorsFormComponent,

    },
    {
        path: ':preview/:doctorId/view',
        component: DoctorsPreviewComponent,

    },
    {
        path: ':preview/:doctorId/setAvailability',
        component: AvailabilityFormComponent,

    },
    {
        path: '**',
        component: DoctorsListComponent,
    }
];

@NgModule({
    declarations: [
        DoctorsListComponent,
        DoctorsFormComponent,
        DoctorsPreviewComponent,
        DoctorSearchListComponent,
        PrescriptionFormComponent,
        AvailabilityFormComponent
    ],
	imports: [
		CommonModule,
		RouterModule.forChild(routes),
		MatProgressBarModule,
		MatSortModule,
		MatTableModule,
		MatIconModule,
		MatNativeDateModule,
		MatButtonModule,
		MatDatepickerModule,
		CdkScrollableModule,
		MatSlideToggleModule,
		MatFormFieldModule,
		MatInputModule,
		MatPaginatorModule,
		MatTooltipModule,
		MatTabsModule,
		FormsModule,
		ReactiveFormsModule,
		MatSelectModule,
		OutsideClickModule,
		SharedModule,
		BarRatingModule
	],
    providers: [
        DoctorService
    ]
})
export class DoctorsModule {
}
