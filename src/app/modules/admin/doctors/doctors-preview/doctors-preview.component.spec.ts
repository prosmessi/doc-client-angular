import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorsPreviewComponent } from './doctors-preview.component';

describe('DoctorsPreviewComponent', () => {
  let component: DoctorsPreviewComponent;
  let fixture: ComponentFixture<DoctorsPreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorsPreviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorsPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
