import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DoctorService} from '../../../../services/doctor/doctor.service';
import {GlobalService} from '../../../../services/global/global.service';
import {ShowErrorService} from '../../../../services/show-error/show-error.service';
import {SpecialityService} from '../../../../services/speciality/speciality.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {browserRefresh} from "../../../../app.component";
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {map, startWith, switchMap} from 'rxjs/operators';
import {MatTableDataSource} from '@angular/material/table';
import {merge} from 'rxjs';
import {fuseAnimations} from "../../../../../@fuse/animations";
@Component({
  selector: 'app-prescription-form',
  templateUrl: './prescription-form.component.html',
  styleUrls: ['./prescription-form.component.scss']
})
export class PrescriptionFormComponent implements OnInit {

  isLoading: boolean = false;
  pageType: string;
  doctorId: string;
  doctorForm: FormGroup;
  doctors: any;
  patientDeatils:any;
  drugs : any;
  data2: any;
 drugsCount: any;
  productForm: FormGroup;
  empForm: FormGroup;
  /**
   * Constructor
   */
  constructor(
      private _router: Router,
      private _activatedRoute: ActivatedRoute,
      private _formBuilder: FormBuilder,
      private _specialityService: SpecialityService,
      private _doctorService: DoctorService,
      private _globalService: GlobalService,
      private _showErrorService: ShowErrorService,
      private _matSnackBar: MatSnackBar
  ) {

  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
      this.getDrugsList(1,30,'');
    //   this.getSpecialityList();
    //   this.pageType = this._activatedRoute.snapshot.paramMap.get('handle') || '';
    //   this.doctorId = this._activatedRoute.snapshot.paramMap.get('doctorId') || '';

    //   this.doctor = this._doctorService._doctorData;
    //   if (browserRefresh && JSON.parse(localStorage.getItem('listItem'))) {
          this.patientDeatils = JSON.parse(localStorage.getItem('listItem'));
          this.doctors = JSON.parse(localStorage.getItem('loggedInUser'));

    //   }
      this.empForm = this._formBuilder.group({
        diagonosis:[this.patientDeatils && this.patientDeatils.diagnostic || '', [Validators.required]],
        ad_advices:[this.patientDeatils && this.patientDeatils.indications_and_notes || '', [Validators.required]],
        // advice:[this.patientDeatils && this.patientDeatils.email || '', [Validators.required]],
        investigation:[this.patientDeatils && this.patientDeatils.conditions_previous || '', [Validators.required]],
        // next_duration:[this.patientDeatils && this.patientDeatils.email || '', [Validators.required]],
        // next_time:[this.patientDeatils && this.patientDeatils.email || '', [Validators.required]],
        patient_name : [this.patientDeatils && this.patientDeatils.first_name || '', [Validators.required]],
        drug_data: this._formBuilder.array([])
      });
      // this.drugs().push(this.newDrugs());
      this.addEmployee(0);
  }

  employees(): FormArray {
    return this.empForm.get('drug_data') as FormArray;
  }

  newEmployee(): FormGroup {
    return this._formBuilder.group({
    //   drugName: '',
      drugName:'',
      morning:'',
      afternoon:'',
      evening:'',
      night:'',
      days:'',
      start_time:'',
      meal:'',
      note:''
    //   skills: this._formBuilder.array([])
    });
  }

  addEmployee(i) {
      // debugger
    this.employees().push(this.newEmployee());
    // this.addEmployeeSkill( this.employees().length-1);

  }

  removeEmployee(empIndex: number) {
    this.employees().removeAt(empIndex);
  }

  employeeSkills(empIndex: number): FormArray {
    return this.employees()
      .at(empIndex)
      .get('skills') as FormArray;
  }

//   newSkill(): FormGroup {
//     return this._formBuilder.group({
//       time_periods0: '',
//       time_periods1: '',
//       time_periods2:'',
//       time_periods3:'',
//       duration_text:'',
//       duration : '',
//       medicine_time :'',
//       note:''
//     });
//   }

//   addEmployeeSkill(empIndex: number) {
//     this.employeeSkills(empIndex).push(this.newSkill());
//   }

//   removeEmployeeSkill(empIndex: number, skillIndex: number) {
//     this.employeeSkills(empIndex).removeAt(skillIndex);
//   }

  onSubmit() {
    console.log(this.empForm.value);
  }

  /**
   * Create New Doctor
   * return {Void}
   */
  createPdf(button): void {
      this.isLoading = true;
      button.target.disabled = true;
    //   const data = this.doctorForm.value;
      const jsonData = {
        doctor_id: this.doctors.users_id+'',
        patient_id: this.patientDeatils.users_id+'',
        email: this.patientDeatils.email,
        call_id: this.patientDeatils.call_id,
        diagnostic: this.empForm.value.diagonosis,
        medicine: JSON.stringify( this.empForm.value.drug_data),
        // treatment: "Strings",
        indications_and_notes:  this.empForm.value.ad_advices,
        conditions_previous:  this.empForm.value.investigation,
        height: this.patientDeatils.pHeight,
        weight: this.patientDeatils.pWeight,
        institute: this.doctors.institution_name,
    };
      // console.log(data);
      this._doctorService.createPdf(jsonData).subscribe(
          (result) => {
              if(result.status_code == 200){
                this._globalService.showMessage('Pdf Send Successfully');
              // this._matSnackBar.open('Pdf Send Successfully', 'OK', this._globalService._matSnackBarConfig);
              this._router.navigate(['/doctors']);
              }else{
                this._globalService.showError(result.status_message);
                // this._matSnackBar.open(result.status_message, 'OK', this._globalService._matSnackBarConfig);
              }
              setTimeout(() => {
                  this.isLoading = false;
                  button.target.disabled = false;
              }, 500);

          },
          (err) => {
              this._showErrorService.showError(err);
              setTimeout(() => {
                  this.isLoading = false;
                  button.target.disabled = false;
              }, 500);
          }
      );
  }

  /**
   * Update Doctor
   * return{void}
   */
  saveDoctor(button): void {
      this.isLoading = true;
      button.target.disabled = true;

      const data = this.doctorForm.value;
      this._doctorService.updateDoctor(data).subscribe(
          () => {
            this._globalService.showMessage('Doctor Updated Successfully');
              // this._matSnackBar.open('Doctor Updated Successfully', 'OK', this._globalService._matSnackBarConfig);
              this._router.navigate(['/doctors']);
              setTimeout(() => {
                  this.isLoading = false;
                  button.target.disabled = false;
              }, 500);
          },
          (err) => {
              this._showErrorService.showError(err);
              setTimeout(() => {
                  this.isLoading = false;
                  button.target.disabled = false;
              }, 500);
          });
  }


  getDrugsList(page,limit,name): void {
      // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
      merge().pipe(
          startWith({}),
          switchMap(() => {
              this.isLoading = true;
              const params = {
                  page: 1,
                  limit: 20,
              };
              return this._doctorService.getdrugsSearch(params.page, params.limit,name);
          }),
          map((data) => {
              this.isLoading = false;
              // set pagination total count
              this.drugsCount = data['result'].length;

              // return response data
              if (data.status_code === 200) {
                  return data['result'];
              }


          })
      ).subscribe(
          (data) => {
              // set response data
              this.drugs = data;
              this.data2 = data;
              // debugger

          },
          (err) => {
              // show the error
              // console.log('err: ', err);
          }
      );
  }

}

