import { AfterViewInit, Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { map, startWith, switchMap } from 'rxjs/operators';
import { FormControl, FormGroup } from '@angular/forms';
import {merge, Observable} from 'rxjs';
import { DoctorService } from '../../../../services/doctor/doctor.service';
import { GlobalService } from '../../../../services/global/global.service';
import { fuseAnimations } from '../../../../../@fuse/animations';
import theme from '@fuse/theme';
import {PaginationService} from '../../../../services/pagination/pagination.service';
import {Pagination} from '../../../../shared/classes/pagination.class';
import {ShowErrorService} from '../../../../services/show-error/show-error.service';


@Component({
    selector: 'app-doctors-list',
    templateUrl: './doctors-list.component.html',
    styleUrls: ['./doctors-list.component.scss'],
    animations: fuseAnimations,
    providers: [PaginationService],
    encapsulation: ViewEncapsulation.None
})
export class DoctorsListComponent extends Pagination implements OnInit, AfterViewInit {

    public httpReq: (page: number, limit: number, queryField: string) => Observable<any>;

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

    isLoading = true;
    role: any;
    doctorsCount: number = 0;
    doctorsTableColumns: string[] = ['name', 'emailPhone', 'speciality', 'experience', 'status', 'createdAt', 'action'];
    doctors: Array<any> = [];
    public theme = theme;
    queryField = '';
    searchField = '';
    filters = '';
    pageIndex = 1;
    totalListCount = 0;
    // limit = 10;
    loadedDoctors = [];
    pageObj = {};
    // filterForm: FormGroup;
    // showFilterOptions = false;


    totalItemsCount = 0;
    limit = 10;
    currentPage = 1;
    currentItems = [];
    // loadedData = [];


    /**
     * Constructor
     */
    constructor(
        private _router: Router,
        private _doctorService: DoctorService,
        public _globalService: GlobalService,
        private _showErrorService: ShowErrorService,
        public _paginationService: PaginationService
    ) {
        super();
        this.httpReq = (page, limit, queryField): Observable<any> => this._globalService.getUserSearch(page, limit, queryField + 'role=doctor');

        this.role = localStorage.getItem('role');
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // this.doctorsCount = 1;
        this._globalService.setState();
    }

    /**
     * After View Init
     */
    async ngAfterViewInit(): Promise<void> {
        await this.fetchForFirstTime();
    }

    /**
     * Get Doctors List
     * return {Void}
     */

    getDoctorsList(): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._doctorService.getDoctors(params.page, params.limit, JSON.parse(localStorage.getItem('loggedInUser')).camp_id);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                this.doctorsCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                this.doctors = data;
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }





    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any {
        return item.id || index;
    }

    /**
     * Edit Doc info
     *
     * @param doctor
     */
    editDoctor(doctor): void {
        localStorage.setItem('listItem', JSON.stringify(doctor));
        this._doctorService._doctorData = doctor;
        this._router.navigate([`/doctors/edit/${doctor.users_id}`]);
    }

    /**
     * Set Doc info
     *
     * @param doctor
     */
    setAvailability(doctor): void {
        localStorage.setItem('listItem', JSON.stringify(doctor));
        this._doctorService._doctorData = doctor;
        this._router.navigate([`/doctors/edit/${doctor.users_id}/setAvailability`]);
    }

    /**
     * Preview doctor's info
     *
     * @param doctor
     */
    previewDoctor(doctor): void {
        localStorage.setItem('listItem', JSON.stringify(doctor));
        this._doctorService._doctorData = doctor;
        this._router.navigate([`/doctors/preview/${doctor.users_id}/view`]);
    }

    /**
     * Delete doctor
     * return{void}
     */
    deleteDoctor(button,id, index): void {

        this.isLoading = true;
        button.target.disabled = true;

        // const data = this.specialityForm.value;
        // const data = {
        //     id:dataObj.id
        // }
        // return;
        this._doctorService.deleteDoctors(id).subscribe(
          () => {
              this._globalService.showMessage('Doctor Deleted Successfully');
              // this._matSnackBar.open('Doctor Deleted Successfully', 'OK', this._globalService._matSnackBarConfig);
              // this._router.navigate(['/speciality']);
              this.deleteItem(index).then(() => {
                  this.isLoading = false;
                  button.target.disabled = false;
              });
              //    delete dataObj;
              // this.doctors =  this.doctors.filter(person => person.users_id !=dataObj );
              //     setTimeout(() => {
              //         this.isLoading = false;
              //         button.target.disabled = false;
              //     }, 500);
          },
          (err) => {
              this._showErrorService.showError(err);
              setTimeout(() => {
                  this.isLoading = false;
                  button.target.disabled = false;
              }, 500);
          });
    }


}
