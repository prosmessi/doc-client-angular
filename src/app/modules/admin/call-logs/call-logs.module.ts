import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CallLogsListComponent} from './call-logs-list/call-logs-list.component';
import {Route, RouterModule} from '@angular/router';
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {CdkScrollableModule} from "@angular/cdk/scrolling";
import {MatTableModule} from "@angular/material/table";
import {MatButtonModule} from "@angular/material/button";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatPaginatorModule} from "@angular/material/paginator";
import {AppointmentService} from "../../../services/appointment/appointment.service";
import {ReactiveFormsModule} from '@angular/forms';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import { CallLogsPreviewComponent } from './call-logs-preview/call-logs-preview.component';
import {DoctorsPreviewComponent} from '../doctors/doctors-preview/doctors-preview.component';

const routes: Route[] = [
    {
        path: '',
        component: CallLogsListComponent
    },
		{
				path: ':preview/:callLogId/view',
				component: CallLogsPreviewComponent,
		}
];

@NgModule({
    declarations: [
        CallLogsListComponent,
        CallLogsPreviewComponent
    ],
	imports: [
		CommonModule,
		RouterModule.forChild(routes),
		MatProgressBarModule,
		MatFormFieldModule,
		MatIconModule,
		MatInputModule,
		CdkScrollableModule,
		MatTableModule,
		MatButtonModule,
		MatTooltipModule,
		MatPaginatorModule,
		ReactiveFormsModule,
		MatDatepickerModule,
		MatNativeDateModule,
	],
    providers: [
        AppointmentService
    ]
})
export class CallLogsModule {
}
