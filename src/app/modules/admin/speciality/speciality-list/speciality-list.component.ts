import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { SpecialityService } from '../../../../services/speciality/speciality.service';
import { MatPaginator } from '@angular/material/paginator';
import { merge, Observable } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { PaginationService } from '../../../../services/pagination/pagination.service';
import theme from '../../../../../@fuse/theme';
import { FormControl, FormGroup } from '@angular/forms';
import { Pagination } from '../../../../shared/classes/pagination.class';
import { ShowErrorService } from '../../../../services/show-error/show-error.service';
import { GlobalService } from '../../../../services/global/global.service';

@Component({
    selector: 'app-speciality-list',
    templateUrl: './speciality-list.component.html',
    styleUrls: ['./speciality-list.component.scss'],
    providers: [PaginationService]
})
export class SpecialityListComponent extends Pagination implements OnInit, AfterViewInit {
    public httpReq: (page: number, limit: number, queryField: string) => Observable<any>;

    specialityTableColumns: string[] = ['name', 'status', 'action'];
    isLoading = true;
    specialities: Array<any> = [];
    specialitiesCount: number = 1;
    public theme = theme;
    filterForm: FormGroup;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

    constructor(
        private _specialityService: SpecialityService,
        public _paginationService: PaginationService,
        public _globalService: GlobalService,
        private _showErrorService: ShowErrorService,
        private _router: Router
    ) {
        super();
        this.httpReq = (page, limit, queryField): Observable<any> => this._specialityService.searchSpeciality(page, limit, queryField);
    }

    ngOnInit(): void {
        this.filterForm = new FormGroup({
            search: new FormControl(null, []),
        });
    }

    resetSearch(e): void {
        e.stopPropagation();
        this.filterForm.reset();
        this.isSearchedList = false;
        this.handleSearchClick(this.filterForm).then();
    }

    /**
     * After View Init
     */
    async ngAfterViewInit(): Promise<void> {
        this.fetchForFirstTime().then();
    }

    /**
     * Get Speciality List
     * return {Void}
     */

    getSpecialityList(): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._specialityService.getSpeciality(params.page, params.limit);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                this.specialitiesCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                this.specialities = data;
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }

    /**
     * Edit Doc info
     *
     * @param doctor
     */
    editSpeciality(doctor): void {
        localStorage.setItem('listItem', JSON.stringify(doctor));
        this._specialityService._specialityData = doctor;
        this._router.navigate([`/speciality/edit/${doctor.id}`]);
    }


    /**
     * Update status
     * return{void}
     */
    saveSpeciality(button, dataObj, index): void {
        this.isLoading = true;
        button.target.disabled = true;

        // const data = this.specialityForm.value;
        const data = {
            id: dataObj.id,
            status: dataObj.is_active
        }
        this._specialityService.updateSpecialityStatus(data).subscribe(
            () => {
                this._globalService.showMessage('Speciality Updated Successfully');
                // this._matSnackBar.open('Speciality Updated Successfully', 'OK', this._globalService._matSnackBarConfig);
                // this._router.navigate(['/speciality']);
                // this.currentItems = this.currentItems.map((c, i) => {
                //     if(index === i) {
                //         return {...c, status: !dataObj.is_active};
                //     }
                //     return c;
                // });
                if (dataObj.is_active === 1) {
                    dataObj.is_active = 0;
                } else {
                    dataObj.is_active = 1;
                }
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);
            },
            (err) => {
                console.log(err);
                this._showErrorService.showError(err);
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);
            });
    }


}
