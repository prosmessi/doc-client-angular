import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-drugs-list',
    templateUrl: './drugs-list.component.html',
    styleUrls: ['./drugs-list.component.scss']
})
export class DrugsListComponent implements OnInit {
    isLoading: boolean = false;
    drugsCount: number = 0;
    drugsTableColumns: string[] = ['sno', 'name', 'type', 'strength', 'action'];

    drugs = [
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'drugs1',
            type: 'CSED',
            strength: 'Tak',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'drugs1',
            type: 'CSED',
            strength: 'Tak',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'drugs1',
            type: 'CSED',
            strength: 'Tak',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'drugs1',
            type: 'CSED',
            strength: 'Tak',
            action: true
        }, {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'drugs1',
            type: 'CSED',
            strength: 'Tak',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'drugs1',
            type: 'CSED',
            strength: 'Tak',
            action: true
        }, {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'drugs1',
            type: 'CSED',
            strength: 'Tak',
            action: true
        },


    ];

    constructor() {
    }

    ngOnInit(): void {
        this.drugsCount = 1;
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any {
        return item.id || index;
    }
}
