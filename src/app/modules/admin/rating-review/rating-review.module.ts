import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RatingReviewComponent} from './rating-review.component';
import {Route, RouterModule} from '@angular/router';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {RatingModule} from 'ng-starrating';
import {BarRatingModule} from 'ngx-bar-rating';
import {MatPaginatorModule} from '@angular/material/paginator';

const routes: Route[] = [
    {
        path: '',
        component: RatingReviewComponent
    }
];

@NgModule({
    declarations: [
        RatingReviewComponent,

    ],
	imports: [
		CommonModule,
		RouterModule.forChild(routes),
		MatProgressBarModule,
		RatingModule,
		BarRatingModule,
		MatPaginatorModule
	]
})
export class RatingReviewModule {
}
