import {Component, OnInit, AfterViewInit} from '@angular/core';
import {StarRatingComponent} from 'ng-starrating';
import {Pagination} from '../../../shared/classes/pagination.class';
import {Observable} from 'rxjs';
import {GlobalService} from '../../../services/global/global.service';

@Component({
    selector: 'app-rating-review',
    templateUrl: './rating-review.component.html',
    styleUrls: ['./rating-review.component.scss']
})
export class RatingReviewComponent extends Pagination implements OnInit, AfterViewInit {
    public httpReq: (page: number, limit: number, queryField: string) => Observable<any>;

    isLoading: boolean = false;
    rateLeft = 4;
    rate = 1;

    constructor(private _globalService: GlobalService) {
        super();
        this.httpReq = (page, limit, queryField): Observable<any> => this._globalService.searchRatings(page, limit, queryField + 'role=doctor');
    }

    ngOnInit(): void {
    }

    ngAfterViewInit(): void {
        this.fetchForFirstTime().then(l => {
            console.log(this.currentItems);
        });
    }

    // Uncomment this function if need to handel rating in admin
    onRate($event: { oldValue: number; newValue: number; starRating: StarRatingComponent }): void {
        /*alert(`Old Value:${$event.oldValue},
      New Value: ${$event.newValue},
      Checked Color: ${$event.starRating.checkedcolor},
      Unchecked Color: ${$event.starRating.uncheckedcolor}`);*/
    }

}
