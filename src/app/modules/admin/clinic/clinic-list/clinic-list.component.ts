import { AfterViewInit, Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { map, startWith, switchMap } from 'rxjs/operators';
import { ClinicService } from '../../../../services/clinic/clinic.service';
import { GlobalService } from '../../../../services/global/global.service';
import { MatTableDataSource } from '@angular/material/table';
import {merge, Observable} from 'rxjs';
// import {DoctorService} from '../../../../services/doctor/doctor.service';
import { fuseAnimations } from '../../../../../@fuse/animations';
import theme from '@fuse/theme';
import { ROUTER_ENUM } from 'app/shared/enums/router.enum';
import {ShowErrorService} from '../../../../services/show-error/show-error.service';
import {PaginationService} from '../../../../services/pagination/pagination.service';
import {Pagination} from '../../../../shared/classes/pagination.class';

@Component({
    selector: 'app-clinic-list',
    templateUrl: './clinic-list.component.html',
    styleUrls: ['./clinic-list.component.scss'],
    animations: fuseAnimations,
    providers: [PaginationService],
    encapsulation: ViewEncapsulation.None
})
export class ClinicListComponent extends Pagination implements OnInit, AfterViewInit {

    public httpReq: (page: number, limit: number, queryField: string) => Observable<any>;

    isLoading = true;
    doctorsCount: number = 0;
    doctorsTableColumns: string[] = ['id', 'namePhone', 'city', 'state', 'enable_payment', 'action'];
    doctors: Array<any> = [];
    public theme = theme;
    public ROUTER_ENUM = ROUTER_ENUM;


    /**
     * Constructor
     */
    constructor(
        private _router: Router,
        // private _doctorService: DoctorService,
        private _clinicService: ClinicService,
        public _globalService: GlobalService,
        private _showErrorService: ShowErrorService
        // public _paginationService: PaginationService
    ) {
        super();
        this.httpReq = (page, limit, queryField): Observable<any> => this._clinicService.getCampSearch(page, limit, queryField);

        // this._paginationService.loadingState$.subscribe({
        //     next: (isLoading) => {
        //         this.isLoading = isLoading;
        //     }
        // });
        //
        // this._paginationService.fetchState$.subscribe({
        //     next: ({cb, params}) => {
        //         this._clinicService.getCampSearch(params.page, params.limit, this._paginationService.queryField)
        //           .subscribe({
        //               next: cb
        //           });
        //     }
        // });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this._globalService.setState();
        this.doctorsCount = 1;
    }

    /**
     * After View Init
     */
    async ngAfterViewInit(): Promise<void> {
        this.fetchForFirstTime().then();
    }

    /**
     * Get Doctors List
     * return {Void}
     */

    getDoctorsList(): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._clinicService.getclinics(params.page, params.limit);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                this.doctorsCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                this.doctors = data;
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }


    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any {
        return item.id || index;
    }

    /**
     * Edit Doc info
     *
     * @param doctor
     */
    editClinic(clinic): void {
        // debugger
        localStorage.setItem('listItem', JSON.stringify(clinic));
        this._clinicService._clinicData = clinic;
        this._router.navigate(['', ROUTER_ENUM.CLINIC, 'edit', clinic.id]);
    }

    /**
     * Preview doctor's info
     *
     * @param doctor
     */
    previewClinic(clinic): void {
        localStorage.setItem('listItem', JSON.stringify(clinic));
        this._clinicService._clinicData = clinic;
        this._router.navigate(['', ROUTER_ENUM.CLINIC, 'preview', clinic.id, 'view']);
        // this._router.navigate([`/clinic/preview/${clinic.id}/view`]);
    }


    deleteClinic(button, id, index): void {
        this.isLoading = true;
        button.target.disabled = true;


        // const data = this.specialityForm.value;
        // const data = {
        //     id:dataObj.id
        // }
        // return;
        this._clinicService.deleteClinic(id).subscribe(
          () => {
              this._globalService.showMessage('Camp Deleted Successfully');
              // this._matSnackBar.open('Doctor Deleted Successfully', 'OK', this._globalService._matSnackBarConfig);
              // this._router.navigate(['/speciality']);
              this.deleteItem(index).then(() => {
                  this.isLoading = false;
                  button.target.disabled = false;
              });
              //    delete dataObj;
              // this.doctors =  this.doctors.filter(person => person.users_id !=dataObj );
              //     setTimeout(() => {
              //         this.isLoading = false;
              //         button.target.disabled = false;
              //     }, 500);
          },
          (err) => {
              this._showErrorService.showError(err);
              setTimeout(() => {
                  this.isLoading = false;
                  button.target.disabled = false;
              }, 500);
          });
    }

}
