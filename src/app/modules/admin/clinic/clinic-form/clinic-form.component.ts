import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClinicService } from '../../../../services/clinic/clinic.service';
import { GlobalService } from '../../../../services/global/global.service';
import { ShowErrorService } from '../../../../services/show-error/show-error.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map, startWith, switchMap } from 'rxjs/operators';
import { browserRefresh } from "../../../../app.component";
import { merge } from 'rxjs';

///delete
import { DoctorService } from '../../../../services/doctor/doctor.service';
import theme from '@fuse/theme';
import { ROUTER_ENUM } from 'app/shared/enums/router.enum';


@Component({
    selector: 'app-clinic-form',
    templateUrl: './clinic-form.component.html',
    styleUrls: ['./clinic-form.component.scss']
})
export class ClinicFormComponent implements OnInit {

    isLoading: boolean = false;
    pageType: string;
    doctorId: string;
    // doctorForm: FormGroup;
    clinicForm: FormGroup;
    clinic: any;
    state: Array<any> = [];
    city: Array<any> = [];
    public theme = theme;
    form: {getCond; getMsg};
    public ROUTER_ENUM = ROUTER_ENUM;
    // specialityCount: number = 0;

    /**
     * Constructor
     */
    constructor(
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _formBuilder: FormBuilder,
        private _clinicService: ClinicService,
        private _doctorService: DoctorService,
        public _globalService: GlobalService,
        private _showErrorService: ShowErrorService,
        private _matSnackBar: MatSnackBar
    ) {
        // this.clinicForm.controls['state'].valueChanges.subscribe((value) => {
        // //     debugger
        //     console.log(value);
        //     // this.models = ... // here you add models to variable models based on selected make
        //   });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.getState();
        this.pageType = this._activatedRoute.snapshot.paramMap.get('handle') || '';
        this.doctorId = this._activatedRoute.snapshot.paramMap.get('doctorId') || '';

        this.clinic = this._clinicService._clinicData;
        if (this.clinic && this.clinic.state && this.clinic.state != '') {
            this.getCity(this.clinic.state);
        }
        if (browserRefresh && JSON.parse(localStorage.getItem('listItem'))) {
            this.clinic = JSON.parse(localStorage.getItem('listItem'));
            if (this.clinic.state && this.clinic.state != '') {
                this.getCity(this.clinic.state);
            }
        }
        if (this.pageType != 'edit') {
            this.clinic = null;
        }
        this.clinicForm = this._formBuilder.group({
            id: [this.clinic && this.clinic.camp_id || ''],
            camp_name: [this.clinic && this.clinic.camp_name || '', [Validators.required, Validators.minLength(3)]],
            tele_phone: [this.clinic && this.clinic.tele_phone || '', [Validators.required, Validators.minLength(10), Validators.pattern('[0-9]+'), Validators.maxLength(11)]],
            // url:[this.clinic && this.clinic.uuid || ''],
            address: [this.clinic && this.clinic.address || '', [Validators.required, Validators.minLength(5)]],
            city: [{value: this.clinic && this.clinic.city || '', disabled: !this.clinic?.city}, [Validators.required]],
            zip_code: [this.clinic && this.clinic.zip_code || '', [Validators.required, Validators.minLength(2), Validators.pattern('[0-9]+')]],
            state: [this.clinic && this.clinic.state || '', [Validators.required]],
            enable_payment: [this.clinic && this.clinic.enable_payment || '', [Validators.required]]
        });
        this.form = {
            getCond: this._globalService.getCondition(this.clinicForm),
            getMsg: this._globalService.getValidationMessage(this.clinicForm)
        };
        // this.createForm();
        this.clinicForm.controls['state'].valueChanges.subscribe((value) => {
            console.log(value);

            this.clinicForm.controls['city'].enable();
            this.getCity(value);
            // this.models = ... // here you add models to variable models based on selected make
        });
    }

    // form(): { getCond; getMsg } {
    //     return {
    //         getCond: this._globalService.getCondition(this.clinicForm),
    //         getMsg: this._globalService.getValidationMessage(this.clinicForm)
    //     };
    // }

    /**
     * Create Doctor form
     *
     * @returns
     */
    // createForm(): void {
    //     this.clinicForm = this._formBuilder.group({
    //         id:[this.clinic && this.clinic.hospital_id || ''],
    //         camp_name:[this.clinic && this.clinic.camp_name || '', [Validators.required]],
    //         tele_phone:[this.clinic && this.clinic.tele_phone || '', [Validators.required]],
    //         // url:[this.clinic && this.clinic.uuid || ''],
    //         address:[this.clinic && this.clinic.address || '', [Validators.required]],
    //         city:[this.clinic && this.clinic.city || '', [Validators.required]],
    //         zip_code:[this.clinic && this.clinic.zip_code || '', [Validators.required]],
    //         state:[this.clinic && this.clinic.state || '', [Validators.required]],
    //         enable_payment:[this.clinic && this.clinic.enable_payment || '', [Validators.required]]
    //     });
    //     // this.doctorForm = this._formBuilder.group({
    //     //     uuid: [this.doctor && this.doctor.uuid || ''],
    //     //     email: [this.doctor && this.doctor.email || '', [Validators.required]],
    //     //     full_name: [this.doctor && this.doctor.name || '', [Validators.required]],
    //     //     short_name: [this.doctor && this.doctor.	first_last_name || ''],
    //     //     phone: [this.doctor && this.doctor.phone || '', [Validators.required]],
    //     //     address: [this.doctor && this.doctor.address || '', [Validators.required]],
    //     //     language: [this.doctor && this.doctor.language || '', [Validators.required]],
    //     //     gender: [this.doctor && this.doctor.gender && this.doctor.gender.toLowerCase() || '', [Validators.required]],
    //     //     speciality: [this.doctor && parseInt(this.doctor.speciality) || '', [Validators.required]],
    //     //     licence: [this.doctor && this.doctor.licence || '', [Validators.required]],
    //     //     image: [''],
    //     //     education: [this.doctor && this.doctor.education || ''],
    //     // });
    // }


    /**
     * Create New Doctor
     * return {Void}
     */
    addClinic(button): void {
        // // eslint-disable-next-line no-debugger
        // debugger;
        this.isLoading = true;
        button.target.disabled = true;
        const data = this.clinicForm.value;
        // console.log(data);
        data.step = '1';
        data.parent_id = 0;
        this._clinicService.addClinic(data).subscribe(
            () => {
                this._globalService.showMessage('New Camp added');
                // this._matSnackBar.open('New Camp added', 'OK', this._globalService._matSnackBarConfig);
                this._router.navigate(['', ROUTER_ENUM.CLINIC]);
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);

            },
            (err) => {
                this._showErrorService.showError(err);
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);
            }
        );
    }

    /**
     * Get Doctors List
     * return {Void}
     */
    // getSpeciality(): void {
    // //     // eslint-disable-next-line no-debugger
    // //     debugger;
    //     this.isLoading = true;
    // //    button.target.disabled = true;
    //  //   const data = this.doctorForm.value;
    //     // console.log(data);
    //     this._doctorService.getSpeciality().subscribe(
    //         () => {
    //             // this._matSnackBar.open('New doctor added', 'OK', this._globalService._matSnackBarConfig);
    //             // this._router.navigate(['/doctors']);
    //             setTimeout(() => {
    //                 this.isLoading = false;
    //                 // button.target.disabled = false;
    //             }, 500);

    //         },
    //         (err) => {
    // //             debugger
    //             this._showErrorService.showError(err);
    //             setTimeout(() => {
    //                 this.isLoading = false;
    //                 // button.target.disabled = false;
    //             }, 500);
    //         }
    //     );
    // }

    getState(): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._doctorService.getState();
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                // this.specialityCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                // debugger
                this.state = data;
                // console.log(this.specialites);
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }

    getCity(state): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._doctorService.getCity(state);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                // this.specialityCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                // debugger
                this.city = data;
                // console.log(this.specialites);
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }

    /**
     * Update Doctor
     * return{void}
     */
    saveClinic(button): void {
        this.isLoading = true;
        button.target.disabled = true;

        const data = this.clinicForm.value;
        // data.name  = data.first_name;
        // data.first_last_name  = data.last_name;
        this._clinicService.updateClinic(data).subscribe(
            (data) => {
                // debugger
                if (data.status_code == 200) {
                    this._globalService.showMessage('Camp Updated Successfully');
                    // this._matSnackBar.open('Camp Updated Successfully', 'OK', this._globalService._matSnackBarConfig);
                    this._router.navigate(['', ROUTER_ENUM.CLINIC]);
                } else {
                    this._globalService.showError(data.status_message);
                    // this._matSnackBar.open(data.status_message, 'OK', this._globalService._matSnackBarConfig);
                }
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);
            },
            (err) => {
                this._showErrorService.showError(err);
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);
            });
    }

    selectState(): void {
        // debugger
    }

    // getCondition(formControlName: string): boolean {
    //     return this.clinicForm.get(formControlName).status === 'INVALID' && (this.clinicForm.get(formControlName).dirty || this.clinicForm.get(formControlName).touched);
    // }
    //
    // getValidationMessage(formControlName: string): IValidationMessageObj {
    //     let isPattern = false;
    //     let message = 'Invalid field';
    //     console.log(this.clinicForm.get(formControlName).errors);
    //     if(this.clinicForm.get(formControlName).errors?.pattern) {
    //         message = 'Invalid Pattern';
    //         isPattern = true;
    //     }
    //     if(this.clinicForm.get(formControlName).errors?.maxlength) {
    //         message = 'Seems too big';
    //     }
    //     if(this.clinicForm.get(formControlName).errors?.required) {
    //         message = 'Required field';
    //     }
    //     if(this.clinicForm.get(formControlName).errors?.minlength) {
    //         message = 'Seems too short';
    //     }
    //
    //     return {
    //         ifPattern: (msg?: string): string => {
    //             if (isPattern && msg) {return msg;}
    //             return message;
    //         },
    //         message
    //     };
    // }
}


/*
* Navigation Toggle button Removed
* Navigation Image Logo size Reduced
* Doctor dashboard Good morning text and date Dynamics
* Doctor form fixed
* Toolbar, Doctor dashboard name word capitalized
*  */
