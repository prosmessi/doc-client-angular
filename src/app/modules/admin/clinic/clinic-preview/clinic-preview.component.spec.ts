import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicPreviewComponent } from './clinic-preview.component';

describe('DoctorsPreviewComponent', () => {
  let component: ClinicPreviewComponent;
  let fixture: ComponentFixture<ClinicPreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClinicPreviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
