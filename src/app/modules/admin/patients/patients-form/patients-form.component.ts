import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { merge } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { GlobalService } from '../../../../services/global/global.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ShowErrorService } from '../../../../services/show-error/show-error.service';
import { PatientsService } from '../../../../services/patients/patients.service';
import { DoctorService } from '../../../../services/doctor/doctor.service';
import theme from '@fuse/theme';

@Component({
    selector: 'app-patients-form',
    templateUrl: './patients-form.component.html',
    styleUrls: ['./patients-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PatientsFormComponent implements OnInit {

    isLoading: boolean = false;
    pageType: string;

    // Forms
    personalInfoForm: FormGroup;
    policyInfoForm: FormGroup;
    role: string;
    state: Array<any> = [];
    city: Array<any> = [];
    patients: any;
    callloglist: any = [];
    patient_id: string;
    page: number = 1;
    limit: number = 10;
    calllogCount: number;
    form: { getCond; getMsg };
    today = new Date();
    public theme = theme;

    /**
     * Constructor
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _formBuilder: FormBuilder,
        private _router: Router,
        private _patientsService: PatientsService,
        private _doctorService: DoctorService,
        private _matSnackBar: MatSnackBar,
        private _showErrorService: ShowErrorService,
        private _globalService: GlobalService,
    ) {
        this.role = localStorage.getItem('role');

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.getState();
        this.pageType = this._activatedRoute.snapshot.paramMap.get('handle');
        // this.createForm();
        // debugger
        this.patients = JSON.parse(localStorage.getItem('listItem'));
        if (this.pageType == 'register') {
            this.patients = null;
        } else if (this.patients.city.length > 0) {
            this.getCity(this.patients.state);
        }
        console.log(this.patients);

        this.personalInfoForm = this._formBuilder.group({
            uuid: [this.patients && this.patients.uuid || ''],
            email: [this.patients && this.patients.email || '', [Validators.required]],
            // title: [this.patients && this.patients.title || ''],
            first_name: [this.patients && this.patients.first_name || '', [Validators.required, Validators.minLength(2)]],
            middle_name: [this.patients && this.patients.middle_name || ''],
            last_name: [this.patients && this.patients.last_name || '', [Validators.required, Validators.minLength(2)]],
            phone: [this.patients && this.patients.phone || '', [Validators.required, Validators.pattern('[0-9]+'), Validators.minLength(10), Validators.maxLength(10)]],
            address1: [this.patients && this.patients.address1 || '', [Validators.required, Validators.minLength(3)]],
            // language: [this.patients && this.patients.language && this.patients.language.charAt(0).toUpperCase() + this.patients.language.substring(1) || '', [Validators.required]],
            gender: [this.patients && this.patients.gender && this.patients.gender.charAt(0).toUpperCase() + this.patients.gender.substring(1) || '', [Validators.required]],
            aadhaar_number: [this.patients && this.patients.aadhaar_number || '', [Validators.required, Validators.minLength(12), Validators.maxLength(12)]],
            // licence: [this.doctor && this.doctor.licence || '', [Validators.required]],
            date_of_birth: [this.patients && this.patients.date_of_birth || '', [Validators.required]],
            height: [this.patients && this.patients.height || '', [Validators.required]],
            weight: [this.patients && this.patients.weight || '', [Validators.required]],
            city: [{ value: this.patients && this.patients.city || '', disabled: !this.patients?.city }, [Validators.required]],
            state: [this.patients && this.patients.state || '', [Validators.required]],
            zip_code: [this.patients && this.patients.zip_code || '', [Validators.required, Validators.pattern('[0-9]+'), Validators.minLength(3), Validators.maxLength(15)]],
            // bio: [this.patients && this.patients.bio || '', [Validators.required]],
            // education: [this.doctor && this.doctor.education || ''],
        });
        this.form = {
            getCond: this._globalService.getCondition(this.personalInfoForm),
            getMsg: this._globalService.getValidationMessage(this.personalInfoForm)
        };
        this.personalInfoForm.controls['state'].valueChanges.subscribe((value) => {
            // debugger
            this.personalInfoForm.controls['city'].patchValue(null);
            this.personalInfoForm.controls['city'].enable();
            this.getCity(value);
            // this.models = ... // here you add models to variable models based on selected make
        });
        this.patient_id = this.patients.id;
        // this.policyInfoForm = this._formBuilder.group({
        //     _id: [''],
        //     policyNumber: ['', [Validators.required]],
        //     policyExpiryDate: [''],
        //     memberId: [''],
        //     companyNumber: [''],
        //     memberActive: [''],
        //     islamicDate: [''],
        //     policyDeductible: [''],
        //     city: [''],
        // });

    }

    // /**
    //  * Create All form
    //  *
    //  * @returns {void}
    //  */
    // createForm(): void {
    //     this.personalInfoForm = this._formBuilder.group({
    //         _id: [''],
    //         dob: ['', [Validators.required]],
    //         gender: [''],
    //         mobile: [''],
    //         userName: [''],
    //         email: [''],
    //         presentAddress: [],
    //         permanentAddress: [''],
    //         national_id: [''],
    //     });
    //     this.policyInfoForm = this._formBuilder.group({
    //         _id: [''],
    //         policyNumber: ['', [Validators.required]],
    //         policyExpiryDate: [''],
    //         memberId: [''],
    //         companyNumber: [''],
    //         memberActive: [''],
    //         islamicDate: [''],
    //         policyDeductible: [''],
    //         city: [''],
    //     });
    // }
    getState(): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._doctorService.getState();
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                // this.specialityCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                // debugger
                this.state = data;
                // console.log(this.specialites);
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }

    getCity(state): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._doctorService.getCity(state);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                // this.specialityCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                // debugger
                this.city = data;
                // console.log(this.specialites);
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }

    /**
     * Create New Doctor
     * return {Void}
     */
    addPatient(button): void {
        // // eslint-disable-next-line no-debugger
        // debugger;
        this.isLoading = true;
        button.target.disabled = true;
        const data = this.personalInfoForm.value;
        // console.log(data);
        // data.year_of_experience = this.educationForm.value.year_of_experience;
        // data.year=this.educationForm.value.year;
        // data.degree = this.educationForm.value.degree;
        // data.licence = this.educationForm.value.licence;
        // data.institution_name = this.educationForm.value.institution_name;
        // data.national_licence = this.educationForm.value.national_licence;
        data.camp_id = JSON.parse(localStorage.getItem('loggedInUser')).camp_id != "0" ? JSON.parse(localStorage.getItem('loggedInUser')).camp_id : '';

        this._patientsService.addPatient(data).subscribe(
            (dataresult) => {
                if (dataresult.status_code == 201) {
                    data.user_id = dataresult.result.insertId;
                    localStorage.setItem('listItem', JSON.stringify(data));
                    this._globalService.showMessage('New patient added');
                    // this._matSnackBar.open('New patient added', 'OK', this._globalService._matSnackBarConfig);
                    this._router.navigate(['/doctors']);
                } else {
                    this._globalService.showError(dataresult.status_message);
                    // this._matSnackBar.open(dataresult.status_message, 'OK', this._globalService._matSnackBarConfig);
                }
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);

            },
            (err) => {
                this._showErrorService.showError(err);
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);
            }
        );
    }


    /**
     * Create New Doctor
     * return {Void}
     */
    updatePatient(button): void {
        // // eslint-disable-next-line no-debugger
        // debugger;
        this.isLoading = true;
        button.target.disabled = true;
        const data = this.personalInfoForm.value;
        // console.log(data);
        // data.year_of_experience = this.educationForm.value.year_of_experience;
        // data.year=this.educationForm.value.year;
        // data.degree = this.educationForm.value.degree;
        // data.licence = this.educationForm.value.licence;
        // data.institution_name = this.educationForm.value.institution_name;
        // data.national_licence = this.educationForm.value.national_licence;
        data.camp_id = JSON.parse(localStorage.getItem('loggedInUser')).camp_id != "0" ? JSON.parse(localStorage.getItem('loggedInUser')).camp_id : '';

        this._patientsService.updatePatient(data).subscribe(
            (dataresult) => {
                if (dataresult.status_code == 200) {
                    data.user_id = dataresult.result.insertId;
                    localStorage.setItem('listItem', JSON.stringify(data));
                    this._globalService.showMessage('Patient updated successfully');
                    // this._matSnackBar.open('New patient added', 'OK', this._globalService._matSnackBarConfig);
                    this._router.navigate(['/patients']);
                } else {
                    this._globalService.showError(dataresult.status_message);
                    // this._matSnackBar.open(dataresult.status_message, 'OK', this._globalService._matSnackBarConfig);
                }
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);

            },
            (err) => {
                this._showErrorService.showError(err);
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);
            }
        );
    }


}
