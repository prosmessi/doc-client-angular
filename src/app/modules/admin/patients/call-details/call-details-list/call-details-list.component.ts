import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';
import {merge} from 'rxjs';
import {map, startWith, switchMap} from 'rxjs/operators';
import {PatientsService} from '../../../../../services/patients/patients.service';

@Component({
    selector: 'app-call-details-list',
    templateUrl: './call-details-list.component.html',
    styleUrls: ['./call-details-list.component.scss']
})
export class CallDetailsListComponent implements OnInit {

    isLoading: boolean = false;
    callloglist : any = [];
    patient_id​: string;
    page​:number = 1;
    limit:number = 10;
    calllogCount : number;
    callLogsTableColumns: string[] = ['name', 'callType', 'date', 'time', 'action'];
    callLogs = [];
    callLogsCount: number = 0;
    patients: any;


    constructor(
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _patientsService: PatientsService) {
    }

    ngOnInit(): void {
        this.patients = JSON.parse(localStorage.getItem('listItem'));
        this.callLogsCount = 2;
        this.patient_id​ = this.patients.id;

        this.getPatientsPastappointment​(this.patient_id​,this.page,this.limit);
    }

      //patient​/past​/appointment​/:patient_id​/:page​/:limit
      getPatientsPastappointment​(patient_id​,page​,limit): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    patient_id​ : patient_id​,
                    page: 1,
                    limit: 20,
                };
                return this._patientsService.getPatientsPastappointment(params.patient_id​, params.page, params.limit);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                this.calllogCount = data['result'].length;
                 // debugger
                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }

            })
        ).subscribe(
            (data) => {
                // set response data
                this.callLogs = data;
                console.log(this.callloglist);
                // debugger
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
}
}
