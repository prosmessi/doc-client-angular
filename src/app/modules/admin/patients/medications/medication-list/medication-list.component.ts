import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-medication-list',
    templateUrl: './medication-list.component.html',
    styleUrls: ['./medication-list.component.scss']
})
export class MedicationListComponent implements OnInit {

    isLoading: boolean = false;
    patients = [
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Inactive',
            createdAt: 'May 24 2021',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Inactive',
            createdAt: 'May 24 2021',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        }, {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        }, {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Inactive',
            createdAt: 'May 24 2021',
            action: true
        }, {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        },


    ];
    medictionsTableColumns: string[] = ['medicineName', 'dosage', 'frequency'];

    constructor() {
    }

    ngOnInit(): void {
    }

}
