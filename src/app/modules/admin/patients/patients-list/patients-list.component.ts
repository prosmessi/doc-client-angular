import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {merge, Observable} from 'rxjs';
import {map, startWith, switchMap} from 'rxjs/operators';
import {PatientsService} from '../../../../services/patients/patients.service';
import {GlobalService} from '../../../../services/global/global.service';
import {Pagination} from '../../../../shared/classes/pagination.class';
import {DoctorService} from '../../../../services/doctor/doctor.service';
import {ShowErrorService} from '../../../../services/show-error/show-error.service';

@Component({
    selector: 'app-patients-list',
    templateUrl: './patients-list.component.html',
    styleUrls: ['./patients-list.component.scss'],
})
export class PatientsListComponent extends Pagination implements OnInit, AfterViewInit {
    public httpReq: (page: number, limit: number, queryField: string) => Observable<any>;

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

    isLoading = true;

    patientsCount: number = 1;
    patientsTableColumns: string[] = ['name', 'emailPhone', 'status', 'createdAt', 'action'];
    patients: Array<any> = [];

    /**
     * Constructor
     */
    constructor(
        private _router: Router,
        private _patientsService: PatientsService,
        private _doctorService: DoctorService,
        private _showErrorService: ShowErrorService,
        public _globalService: GlobalService,
    ) {
        super();
        this.httpReq = (page, limit, queryField): Observable<any> => this._globalService.getUserSearch(page, limit, queryField + 'role=patient');
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this._globalService.setState();
    }

    /**
     * After View Init
     */
    async ngAfterViewInit(): Promise<void> {
        await this.fetchForFirstTime();
    }

    /**
     * Get Patients List
     * return {Void}
     */

    getPatientsList(): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._patientsService.getPatients(params.page, params.limit,JSON.parse(localStorage.getItem('loggedInUser')).camp_id);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                this.patientsCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }

            })
        ).subscribe(
            (data) => {
                // set response data
                this.patients = data;
                console.log(this.patients);
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }


    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any {
        return item.id || index;
    }

    /**
     * Edit Doc info
     *
     * @param patient
     */
    editPatient(patient): void {
        localStorage.setItem('listItem', JSON.stringify(patient));
        this._patientsService._patientData = patient;
        this._router.navigate([`/patients/edit/${patient.users_id}`]);
    }

    /**
     * Preview doctor's info
     *
     * @param patient
     */
    previewPatient(patient): void {
        localStorage.setItem('listItem', JSON.stringify(patient));
        this._patientsService._patientData = patient;
        this._router.navigate([`/patients/preview/${patient.users_id}/view`]);
    }

    /**
     * delete agent
     *
     * @param agent
     */
    deleteDoctor(button,dataObj, index): void {

        this.isLoading = true;
        button.target.disabled = true;

        // const data = this.specialityForm.value;
        // const data = {
        //     id:dataObj.id
        // }
        //Sync with the pagination logic
        this._doctorService.deleteDoctors(dataObj).subscribe(
          () => {
              this._globalService.showMessage('Patient Deleted Successfully');
              // this._matSnackBar.open('Representative Deleted Successfully', 'OK', this._globalService._matSnackBarConfig);
              // this._router.navigate(['/speciality']);
              //    delete dataObj;
              this.deleteItem(index).then(() => {
                  this.isLoading = false;
                  button.target.disabled = false;
              });
              // this.currentItems =  this.currentItems.filter(person => person.users_id !=dataObj );
              //     setTimeout(() => {
              //         this.isLoading = false;
              //         button.target.disabled = false;
              //     }, 500);
          },
          (err) => {
              this._showErrorService.showError(err);
              setTimeout(() => {
                  this.isLoading = false;
                  button.target.disabled = false;
              }, 500);
          });
    }

}
