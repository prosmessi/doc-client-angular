import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientsPreviewComponent } from './patients-preview.component';

describe('PatientsPreviewComponent', () => {
  let component: PatientsPreviewComponent;
  let fixture: ComponentFixture<PatientsPreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientsPreviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientsPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
