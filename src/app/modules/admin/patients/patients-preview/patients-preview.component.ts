import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DoctorService} from '../../../../services/doctor/doctor.service';
import {browserRefresh} from '../../../../app.component';
import {PatientsService} from '../../../../services/patients/patients.service';

@Component({
  selector: 'app-patients-preview',
  templateUrl: './patients-preview.component.html',
  styleUrls: ['./patients-preview.component.scss']
})
export class PatientsPreviewComponent implements OnInit {

  isLoading: boolean = false;
  doctorId: string;
  doctor: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _patientsService: PatientsService
  ) { }

  ngOnInit(): void {
    this.doctorId = this._activatedRoute.snapshot.paramMap.get('doctorId') || '';

    // this.doctor = this._patientsService._patientData;
    // if (browserRefresh && JSON.parse(localStorage.getItem('listItem'))) {
    //   this.doctor = JSON.parse(localStorage.getItem('listItem'));
    // }
    this.doctor = this._patientsService._patientData;
    // console.log(this.doctor);
    // if(this.doctor.education_details) {
    //   this.doctor.education_details = JSON.parse(this.doctor.education_details);
    // }
  }

}
