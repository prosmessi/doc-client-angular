import { AfterViewInit, Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { map, startWith, switchMap } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material/table';
import {merge, Observable} from 'rxjs';
import { DoctorService } from '../../../../services/doctor/doctor.service';
import { fuseAnimations } from '../../../../../@fuse/animations';
import {GlobalService} from '../../../../services/global/global.service';
import theme from '@fuse/theme';
import {Pagination} from '../../../../shared/classes/pagination.class';
import {ShowErrorService} from '../../../../services/show-error/show-error.service';

@Component({
    selector: 'app-agent-list',
    templateUrl: './agent-list.component.html',
    styleUrls: ['./agent-list.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class AgentListComponent extends Pagination implements OnInit, AfterViewInit {
    public httpReq: (page: number, limit: number, queryField: string) => Observable<any>;

    isLoading = true;
    agentCount: number = 0;
    doctorsTableColumns: string[] = ['name', 'emailPhone', 'city', 'aadhaar_number', 'camp_name', 'action'];
    agents: Array<any>;
    public theme = theme;

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;


    /**
     * Constructor
     */
    constructor(
        private _router: Router,
        private _doctorService: DoctorService,
        public _globalService: GlobalService,
        private _showErrorService: ShowErrorService,
    ) {
        super();
        this.httpReq = (page, limit, queryField): Observable<any> => this._globalService.getUserSearch(page, limit, queryField + 'role=representative');
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.agentCount = 1;
        this._globalService.setState();

        // this._paginationService.loadingState$.subscribe({
        //     next: (isLoading) => {
        //         this.isLoading = isLoading;
        //     }
        // });
        //
        // this._paginationService.fetchState$.subscribe({
        //     next: ({cb, params}) => {
        //         this._globalService.getUserSearch(params.page, params.limit, this._paginationService.queryField + 'role=representative')
        //           .subscribe({
        //               next: cb
        //           });
        //     }
        // });
    }

    /**
     * After View Init
     */
    async ngAfterViewInit(): Promise<void> {
        await this.fetchForFirstTime();
        // await this.fetchFromDB(this.currentPage - 1, this.limit);
        // this.currentItems = this.loadedData;
        // this.getSearchedDoctors(this.currentPage, this.limit);
    }

    /**
     * Get Doctors List
     * return {Void}
     */

    getAgentList(): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._doctorService.getagent(params.page, params.limit);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                this.agentCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                this.agents = data;
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }


    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any {
        return item.id || index;
    }

    /**
     * Edit Doc info
     *
     * @param doctor
     */
    editDoctor(doctor): void {
        localStorage.setItem('listItem', JSON.stringify(doctor));
        this._doctorService._doctorData = doctor;
        this._router.navigate([`/agent/edit/${doctor.users_id}`]);
    }

    /**
     * Preview doctor's info
     *
     * @param doctor
     */
    previewDoctor(doctor): void {
        localStorage.setItem('listItem', JSON.stringify(doctor));
        this._doctorService._doctorData = doctor;
        this._router.navigate([`/agent/preview/${doctor.users_id}/view`]);
    }

    /**
     * delete agent
     *
     * @param agent
     */
    deleteDoctor(button,dataObj, index): void {

        this.isLoading = true;
        button.target.disabled = true;

        // const data = this.specialityForm.value;
        // const data = {
        //     id:dataObj.id
        // }
        //Sync with the pagination logic
        this._doctorService.deleteDoctors(dataObj).subscribe(
          () => {
              this._globalService.showMessage('Representative Deleted Successfully');
              // this._matSnackBar.open('Representative Deleted Successfully', 'OK', this._globalService._matSnackBarConfig);
              // this._router.navigate(['/speciality']);
              //    delete dataObj;
              this.deleteItem(index).then(() => {
                  this.isLoading = false;
                  button.target.disabled = false;
              });
              // this.currentItems =  this.currentItems.filter(person => person.users_id !=dataObj );
              //     setTimeout(() => {
              //         this.isLoading = false;
              //         button.target.disabled = false;
              //     }, 500);
          },
          (err) => {
              this._showErrorService.showError(err);
              setTimeout(() => {
                  this.isLoading = false;
                  button.target.disabled = false;
              }, 500);
          });
    }


}
