import { Route } from '@angular/router';
import { AuthGuard } from './core/auth/guards/auth.guard';
import { InitialDataResolver } from './app.resolvers';
import { LayoutComponent } from './layout/layout.component';
import { ROUTER_ENUM } from './shared/enums/router.enum';

// @formatter:off
// tslint:disable:max-line-length
export const appRoutes: Route[] = [

    // Redirect empty path to '/dashboard', then it will verify for loggedIn status
    { path: ROUTER_ENUM.BASE, pathMatch: 'full', redirectTo: ROUTER_ENUM.DASHBOARD },

    // Auth routes
    {
        path: ROUTER_ENUM.BASE,
        children: [
            {
                path: ROUTER_ENUM.FORGOT_PASSWORD,
                loadChildren: () => import('./modules/auth/forgot-password/forgot-password.module').then(m => m.AuthForgotPasswordModule)
            },
            {
                path: ROUTER_ENUM.SIGN_IN,
                loadChildren: () => import('./modules/auth/sign-in/sign-in.module').then(m => m.AuthSignInModule)
            },
        ]
    },

    // Admin routes
    {
        path: '',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        component: LayoutComponent,
        resolve: {
            initialData: InitialDataResolver,
        },
        children: [
            {
                path: ROUTER_ENUM.DASHBOARD,
                loadChildren: () => import('./modules/admin/dashboard/dashboard.module').then(m => m.DashboardModule)
            },
            {
                path: ROUTER_ENUM.DOCTORS,
                loadChildren: () => import('./modules/admin/doctors/doctors.module').then(m => m.DoctorsModule)
            },
            {
                path: ROUTER_ENUM.AGENT,
                loadChildren: () => import('./modules/admin/agent/agent.module').then(m => m.AgentModule)
            },
            {
                path: ROUTER_ENUM.PATIENTS,
                loadChildren: () => import('./modules/admin/patients/patients.module').then(m => m.PatientsModule)
            },
            {
                path: ROUTER_ENUM.APPOINTMENTS,
                loadChildren: () => import('./modules/admin/appointments/appointments.module').then(m => m.AppointmentsModule)
            },
            {
                path: ROUTER_ENUM.CALL_QUEUE,
                loadChildren: () => import('./modules/admin/call-queue/call-queue.module').then(m => m.CallQueueModule)
            },
            {
                path: ROUTER_ENUM.RATING_REVIEW,
                loadChildren: () => import('./modules/admin/rating-review/rating-review.module').then(m => m.RatingReviewModule)
            },
            {
                path: ROUTER_ENUM.CLINIC,
                loadChildren: () => import('app/modules/admin/clinic/clinic.module').then(m => m.ClinicModule)
            },
            // {
            //     path: 'drugs',
            //     loadChildren: () => import('app/modules/admin/drugs/drugs.module').then(m => m.DrugsModule)
            // },
            {
                path: ROUTER_ENUM.CALL_LOGS,
                loadChildren: () => import('./modules/admin/call-logs/call-logs.module').then(m => m.CallLogsModule)
            },
            {
                path: ROUTER_ENUM.SPECIALITY,
                loadChildren: () => import('./modules/admin/speciality/speciality.module').then(m => m.SpecialityModule)
            },
            {
                path: ROUTER_ENUM.RESET_PASSWORD,
                loadChildren: () => import('./modules/admin/reset-password/reset-password.module').then(m => m.AuthResetPasswordModule)
            },
        ]
    }
];
