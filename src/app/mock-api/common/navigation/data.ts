/* tslint:disable:max-line-length */
import { FuseNavigationItem } from '@fuse/components/navigation';
import { ROUTER_ENUM } from 'app/shared/enums/router.enum';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id: 'dashboard',
        title: 'Dashboard',
        type: 'basic',
        icon: 'mat_outline:dashboard',
        link: ROUTER_ENUM.DASHBOARD,
        action: 'representative,admin,doctor'
    },
    {
        id: 'clinic',
        title: 'Camps',
        type: 'basic',
        icon: 'local_hotel',
        link: ROUTER_ENUM.CLINIC,
        action: 'admin'
    },
    {
        id: 'agent',
        title: 'Representatives',
        type: 'basic',
        icon: 'person_pin',
        link: ROUTER_ENUM.AGENT,
        action: 'admin'
    },
    {
        id: 'doctors',
        title: 'Doctors',
        type: 'basic',
        icon: 'iconsmind:doctor',
        link: ROUTER_ENUM.DOCTORS,
        action: 'admin,representative'
    },
    {
        id: 'patient',
        title: 'Patients',
        type: 'basic',
        icon: 'heroicons_outline:user',
        link: ROUTER_ENUM.PATIENTS,
        action: 'admin,doctor'
    },
    {
        id: 'register',
        title: 'Patient Register',
        type: 'basic',
        icon: 'heroicons_outline:user',
        link: '/patients/register',
        action: 'representative'
    },
    {
        id: 'psearch',
        title: 'Patient Search',
        type: 'basic',
        icon: 'heroicons_outline:user',
        link: '/patients/search/list',
        action: 'representative'
    },
    {
        id: 'appointment',
        title: 'Appointments',
        type: 'basic',
        icon: 'iconsmind:calendar_clock',
        link: ROUTER_ENUM.APPOINTMENTS,
        action: 'representative,admin,doctor'
    },
    {
        id: 'call-queue',
        title: 'Call Queue',
        type: 'basic',
        icon: 'queue',
        link: ROUTER_ENUM.CALL_QUEUE,
        action: 'admin,doctor'
    },
    {
        id: 'rating-review',
        title: 'Ratings & Reviews',
        type: 'basic',
        icon: 'rate_review',
        link: ROUTER_ENUM.RATING_REVIEW,
        action: 'admin,doctor'
    },
    //    {
    //         id: 'prescription',
    //         title: 'Prescription',
    //         type: 'basic',
    //         icon: 'iconsmind:notepad',
    //         link: '/doctors/create/prescription',
    //         action: 'doctor'
    //     },
    {
        id: 'speciality',
        title: 'Specialities',
        type: 'basic',
        icon: 'mat_solid:folder_special',
        link: ROUTER_ENUM.SPECIALITY,
        action: 'admin'
    },
    {
        id: 'call-logs',
        title: 'Call Logs',
        type: 'basic',
        icon: 'mat_outline:add_ic_call',
        link: ROUTER_ENUM.CALL_LOGS,
        action: 'representative,admin,doctor'
    },
    {
        id: 'change-password',
        title: 'Change Password',
        type: 'basic',
        icon: 'iconsmind:password_field',
        link: ROUTER_ENUM.RESET_PASSWORD,
        action: 'representative,admin,doctor'
    },
    {
        id: 'logout',
        title: 'Logout',
        type: 'basic',
        icon: 'heroicons_outline:logout',
        link: ROUTER_ENUM.SIGN_IN,
        action: 'representative,admin,doctor'
    }

];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
