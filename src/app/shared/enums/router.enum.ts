export enum ROUTER_ENUM {
    DASHBOARD = 'dashboard',
    DOCTORS = 'doctors',
    AGENT = 'agent',
    PATIENTS = 'patients',
    APPOINTMENTS = 'appointments',
    CALL_QUEUE = 'call-queue',
    RATING_REVIEW = 'rating-review',
    CLINIC = 'camp',
    CALL_LOGS = 'call-logs',
    SPECIALITY = 'speciality',
    RESET_PASSWORD = 'reset-password',
    FORGOT_PASSWORD = 'forgot-password',
    SIGN_IN = 'sign-in',
    BASE = ''
}