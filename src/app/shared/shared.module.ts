import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppInputInfoComponent } from './components/app-input-info/app-input-info.component';
import { AdminHeaderComponent } from './components/admin-header/admin-header.component';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {RouterModule} from '@angular/router';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		MatIconModule,
		MatFormFieldModule,
		MatProgressBarModule,
		RouterModule,
		MatButtonModule,
		MatSelectModule,
		MatInputModule
	],
	exports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		AppInputInfoComponent,
		AdminHeaderComponent
	],
    declarations: [
      AppInputInfoComponent,
      AdminHeaderComponent
    ]
})
export class SharedModule
{
}
