import {Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {PaginationService} from '../../../services/pagination/pagination.service';
import {GlobalService} from '../../../services/global/global.service';
import {fuseAnimations} from '../../../../@fuse/animations';
import {MatButton} from '@angular/material/button';
import {ClinicService} from '../../../services/clinic/clinic.service';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss'],
  animations: fuseAnimations
})
export class AdminHeaderComponent implements OnInit {

  @Input() isLoading: boolean;
  @Input() campFilter: boolean = false;
  @Input() isSearchedList: boolean;
  @Input() noStatus: boolean = false;
  @Input() enablePayment: boolean = false;
  @Input() routerLinkForNewButton: any;
  @Input() shouldRenderNewButton: boolean;
  @Input() headerTitle: string;
  @Output() handleSearch = new EventEmitter();

  // @ViewChild('searchBtn') searchBtn: MatButton;

  filterForm: FormGroup;
  showFilterOptions = false;
  camps: Array<any> = [];
  campCount: number = 0;
  selectedTabIndex: number = 0;
  timer: any;

  @ViewChild('input') inputSearchCamps: ElementRef<HTMLInputElement>;

  constructor(
    // public _paginationService: PaginationService,
    public _globalService: GlobalService,
    public _clinicService: ClinicService
  ) { }

  // @HostListener('document:keypress', ['$event'])
  // onSelectionChange(): void {
  //   console.log('[admin-header.component.ts || Line no. 34 ....]', 'Selection changed -- -- ', this.searchBtn);
  //   this.searchBtn._elementRef.nativeElement.focus();
  // }

  ngOnInit(): void {
    this.filterForm = new FormGroup({
      search: new FormControl(null, []),
      state: new FormControl(null, []),
      city: new FormControl(null, []),
      experience: new FormControl(null, []),
      camp_id: new FormControl(null, []),
      // eslint-disable-next-line @typescript-eslint/naming-convention
      is_active: new FormControl(null, []),
      // eslint-disable-next-line @typescript-eslint/naming-convention
      enable_payment: new FormControl(null, []),
    });
    this.setCamps();
    this.filterForm.controls['city'].disable();
    this.filterForm.controls['state'].valueChanges.subscribe((value) => {
      // // debugger
      console.log(value);

      if(value === null) {
        this.filterForm.controls['city'].reset();
        this.filterForm.controls['city'].disable();
      } else {
        this._globalService.setCities(value);
        this.filterForm.controls['city'].enable();
      }
      // this.models = ... // here you add models to variable models based on selected make
    });
  }

  // public handleEnter(e): void {
  //   console.log('[admin-header.component.ts || Line no. 58 ....]', 'Key pressed on select')
  //   if(e.key === 'Enter') {
  //     this._paginationService.handleSearchClick(this.filterForm).then();
  //   }
  // }

  resetSearch(e): void {
    e.stopPropagation();
    this.filterForm.reset();
    this.isSearchedList = false;
    this.handleSearch.emit();
  }


  toggleFilterOptions(): void {
    this.showFilterOptions = !this.showFilterOptions;
  }

  clear(e, controlName): void {
    e.stopPropagation();
    this.filterForm.controls[controlName].reset();
    // console.log('Nice one', controlName);
  }

  selectSpecialitiesOpened(): void {
    this.inputSearchCamps.nativeElement.focus();
  }

  selectSpecialitiesClosed(): void {
    this.inputSearchCamps.nativeElement.value = '';
    this.setCamps();
  }

  onKey({target: {value}}): void {
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(() => {
      this.setCamps(value);
    }, 400);
  }

  setCamps(value = ''): void {
    this._clinicService.getCampSearch(0, 20, 'search=' + value).subscribe({
      next: (data) => {
        this.isLoading = false;
        console.log('[agent-form.component.ts || Line no. 234 ....]', data);
        if (data.status_code === 200) {
          this.camps = data['result'].data;
          // this.totalItemsCount = data['result'].count;
          // this.loadedData.push(...data['result'].data);
        }
        if (data.status_code === 404) {
          this.camps = [];
        }
      }
    });
  }


}
