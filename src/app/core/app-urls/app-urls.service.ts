import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AppUrlsService {

    constructor() { }


    public get appUrl(): string {
        return environment.baseUrl;
    }

    public get appUrlAdmin(): string {
        return environment.baseUrl + 'admin/';
    }

    public updateSpecialityStatus(specialityId): string {
        return this.appUrl + `specialities/activeInactive/${specialityId}`;
    }

    public login(): string {
        return this.appUrlAdmin + 'login';
    }

    public doctor(page, limit, campId): string {
        return this.appUrlAdmin + `getDoctorList/${campId}/${page}/${limit}`;
    }

    public getDoctorRating(page, limit, doctorId): string {
        return this.appUrl + `api/search-ratings/${page}/${limit}?doctor_id=${doctorId}`;
    }
    public doctorSearch(page, limit, spec, name): string {
        return this.appUrl + `doctor/Search/${page}/${limit}/${spec}/${name}`;
    }
    public userSearch(page, limit, queryField): string {
        return this.appUrl + `api/search-user/${page}/${limit}?${queryField}`;
    }
    public campSearch(page, limit, queryField): string {
        return this.appUrl + `api/search-camp/${page}/${limit}?${queryField}`;
    }
    public drugsSearch(page, limit, name): string {
        return this.appUrl + `drugs/Search/${page}/${limit}/${name}`;
    }
    public agent(page, limit): string {
        return this.appUrlAdmin + `getAgentList/${page}/${limit}`;
    }
    public searchAppointments(page, limit, queryField): string {
        return this.appUrl + `api/search-appointment/${page}/${limit}?${queryField}`;
    }
    public searchCallLogs(page, limit, queryField): string {
        return this.appUrl + `api/search-calllog/${page}/${limit}?${queryField}`;
    }
    public searchRatings(page, limit, queryField): string {
        return this.appUrl + `api/search-ratings/${page}/${limit}?${queryField}`;
    }

    public clinic(page, limit): string {
        return this.appUrl + `camp/allList/${page}/${limit}`;
    }
    public activeSpeciality(page, limit): string {
        return environment.baseUrl + `patient/specialities/list`;
    }

    public city(state): string {
        return environment.baseUrl + `admin/city/${state}`;
    }

    public state(): string {
        return environment.baseUrl + 'admin/state';
    }
    // public clinic(): string {
    //     return environment.baseUrl + `admin/state`;
    // }

    public addDoctor(): string {
        return this.appUrl + 'doctor/signup';
    }
    public createPdf(): string {
        return this.appUrl + 'camp/createPdf';
    }
    public updateAvailability(uuid): string {
        return this.appUrl + 'doctor/updateAvailability/' + uuid;
    }
    public addPatient(): string {
        return this.appUrlAdmin + 'addPatient';
    }
    public updatePatient(uuid): string {
        return this.appUrl + 'patient/updateProfile/' + uuid;
    }
    public makeCall(): string {
        return this.appUrl + 'patient/makeCall';
    }
    public addAgent(): string {
        return this.appUrl + 'representative/signUp';
    }
    public forgetPassword(email): string {
        return this.appUrlAdmin + 'forgot/password/' + email;
    }
    public changePassword(): string {
        return this.appUrlAdmin + 'resetPassword';
    }
    public changeByOldPassword(): string {
        return this.appUrl + 'api/change-password';
    }
    public verifyOtp(email, otp): string {
        return this.appUrl + 'user/verifyOtp/' + email + '/' + otp;
    }
    public addClinic(): string {
        return this.appUrl + 'camp/createCamp';
    }
    public agentId(agentId): string {
        return this.appUrl + `agent/update/${agentId}`;
    }
    public doctorId(doctorId): string {
        return this.appUrl + `doctor/update/${doctorId}`;
    }
    public deleteuser(id): string {
        return this.appUrl + `user/delete/${id}`;
    }

    public deleteAppointment(id): string {
        return this.appUrl + `appointment/delete/${id}`;
    }

    public campId(campId): string {
        return this.appUrl + `camp/update/${campId}`;
    }
    public deleteCamp(campId): string {
        return this.appUrl + `camp/delete/${campId}`;
    }

    public patients(page, limit, campId): string {
        return this.appUrlAdmin + `getPatientList/${campId}/${page}/${limit}`;
    }

    public patientsSearch(page, limit, name): string {
        return this.appUrl + `patient/Search/${page}/${limit}/${name}`;
    }

    public getPatientsPastappointment(patient_id, page, limit): string {
        return environment.baseUrl + `patient/past/appointment/${patient_id}/${page}/${limit}`;
    }

    public appointments(page, limit, campId): string {
        return this.appUrlAdmin + `appointment/upcoming/${page}/${limit}/${campId}`;
    }

    public pastAppointments(page, limit, campId): string {
        return this.appUrlAdmin + `appointment/past/${page}/${limit}/${campId}`;
    }

    public speciality(page, limit): string {
        return this.appUrlAdmin + `speciality`;
    }

    public searchSpeciality(page, limit, queryField): string {
        return this.appUrl + `api/search-speciality/${page}/${limit}?${queryField}`;
    }

    public addSpeciality(): string {
        return this.appUrlAdmin + 'add/speciality';
    }

    public updateSpeciality(specialityId): string {
        return this.appUrlAdmin + `activeInactive/speciality/${specialityId}`;
    }

    //
    public callWaitingList(uuid): string {
        return environment.baseUrl + `doctor/callWaitingList/${uuid}`;
        // return this.appUrlAdmin + `doctor/callWaitingList​/${uuid}`;
    }

    //getRtcAccessToken
    public getRtcAccessToken(): string {
        return environment.baseUrl + `common/getRtcAccessToken`;
    }

    //updateCallStatus
    public updateCallStatus(): string {
        return environment.baseUrl + `doctor/updated/callStatus`;
    }

    //Get Dashboard count
    public getDashboardCount(): string {
        return environment.baseUrl + `admin/dashboard/count`;
    }


}
