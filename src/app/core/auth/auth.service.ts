import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {catchError, switchMap} from 'rxjs/operators';
import {UserService} from 'app/core/user/user.service';
import {AppUrlsService} from '../app-urls/app-urls.service';

@Injectable()
export class AuthService {
    public _authenticated: boolean = false;

    /**
     * Constructor
     */
    constructor(
        private _httpClient: HttpClient,
        private _userService: UserService,
        private _appUrlService: AppUrlsService
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Setter & getter for access token
     */
    set loggedInStatus(value) {
        localStorage.setItem('isLoggedIn', value);
    }

    get loggedInStatus(): string {
        return localStorage.getItem('isLoggedIn') ?? '';
    }

    /**
     * Setter for loggedIn user role
     */
    set loggedInUserRole(role) {
        localStorage.setItem('role', role);
    }

    get loggedInUserRole(): string {
        return localStorage.getItem('role') ?? '';
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Forgot password
     *
     * @param email
     */
    forgotPassword({email}): Observable<any> {
        return this._httpClient.get(this._appUrlService.forgetPassword(email));
    }
    /**
     * Forgot password
     *
     * @param data
     */
    changePassword(data): Observable<any> {
        return this._httpClient.post(this._appUrlService.changePassword(), data);
    }
    /**
     * Forgot password
     *
     * @param data
     */
    changeByOldPassword(data): Observable<any> {
        return this._httpClient.post(this._appUrlService.changeByOldPassword(), data);
    }
    /**
     * Verify Otp
     *
     * @param email
     * @param otp
     */
    verifyOtp({email, otp}): Observable<any> {
        return this._httpClient.get(this._appUrlService.verifyOtp(email, otp));
    }

    /**
     * Reset password
     *
     * @param password
     */
    resetPassword(password: string): Observable<any> {
        return this._httpClient.post('api/auth/reset-password', password);
    }

    /**
     * Sign in
     *
     * @param credentials
     */
    signIn(credentials): Observable<any> {
        return this._httpClient.post(this._appUrlService.login(), credentials).pipe(
            switchMap((response: any) => {
                if (response.status_code === 200) {
                    // Store the Login status in the local storage
                    this.loggedInStatus = String(true);
                    this.loggedInUserRole = credentials.type;

                    // Set the authenticated flag to true
                    this._authenticated = true;
                    // Store the user on the user service
                    localStorage.setItem('loggedInUser', JSON.stringify(response.result));
                    this._userService.user = response.result;

                }


                // Return a new observable with the response
                return of(response);

            })
        );
    }

    /**
     * Sign out
     */
    signOut(): Observable<any> {
        // Remove the access token from the local storage
        localStorage.removeItem('isLoggedIn');

        // Set the authenticated flag to false
        this._authenticated = false;

        // Return the observable
        return of(true);
    }

    /**
     * Sign up
     *
     * @param user
     */
    signUp(user: { name: string; email: string; password: string; company: string }): Observable<any> {
        return this._httpClient.post('api/auth/sign-up', user);
    }

    /**
     * Unlock session
     *
     * @param credentials
     */
    unlockSession(credentials: { email: string; password: string }): Observable<any> {
        return this._httpClient.post('api/auth/unlock-session', credentials);
    }

    /**
     * Check the authentication status
     */
    check(): Observable<boolean> {
        // Check the access token availability
        if (!this.loggedInStatus) {
            return of(false);
        }

        if (!this.loggedInUserRole) {
            return of(false);
        }

        return of(true);

        // Check if the user is logged in
        /*if (this._authenticated) {
            return of(true);
        }*/

    }
}
